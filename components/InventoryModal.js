import React from 'react';
import { View, Modal, Button } from 'react-native';
import PropTypes from 'prop-types';
import { StackNavigator } from 'react-navigation';

import InventoryController from './InventoryController';

export default class InventoryModal extends React.Component {
  render() {
    const InventoryNavigator = StackNavigator({
      Controller: {
        screen: InventoryController,
        navigationOptions: ({ navigation }) => ({
          headerLeft: (
            <Button
              onPress={() => {
                this.props.suspendInventoryCb();
              }}
              title="Close"
            />
          ),
        }),
      },
    });
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.visible}
        onRequestClose={() => {
          this.props.suspendInventoryCb();
        }}>
        <View style={{ height: '100%' }}>
          <InventoryNavigator screenProps={{ endInventoryCb: this.props.endInventoryCb }} />
        </View>
      </Modal>
    );
  }
}

InventoryModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  endInventoryCb: PropTypes.func.isRequired,
  suspendInventoryCb: PropTypes.func.isRequired,
};
