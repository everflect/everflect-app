import React from 'react';
import { Dimensions, View } from 'react-native';
import PropTypes from 'prop-types';
import ProgressBar from './ProgressBar';

const PROGRESS_WIDTH = Dimensions.get('window').width - 20;

const progressFillStyle = {};
const progressBackgroundStyle = {
  backgroundColor: '#cccccc',
  borderRadius: 2,
};
const progressStyle = {
  marginTop: 10,
  marginBottom: 10,
  width: PROGRESS_WIDTH,
};

export default class InventoryHead extends React.Component {
  render() {
    if (!this.props.data.loading) {
      const DENOMINATOR = 13.0;
      const NUMERATOR =
        this.props.data.currentInventory === null
          ? DENOMINATOR
          : this.props.data.currentInventory.myStep;
      return (
        <View style={{ marginHorizontal: 10 }}>
          <ProgressBar
            fillStyle={progressFillStyle}
            backgroundStyle={progressBackgroundStyle}
            style={progressStyle}
            progress={NUMERATOR / DENOMINATOR}
          />
        </View>
      );
    } else {
      return null;
    }
  }
}

InventoryHead.propTypes = {
  data: PropTypes.object.isRequired,
};
