import React from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import { Cell, Separator } from 'react-native-tableview-simple';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

class SettingsPageBase extends React.Component {
  render() {
    const data = [{ id: 'qanda' }, { id: 'licenses' }, { id: 'signout' }];
    return (
      <FlatList
        style={{ backgroundColor: 'white' }}
        data={data}
        keyExtractor={(item, index) => item.id}
        renderItem={({ item, separators }) => {
          switch (item.id) {
            case 'qanda':
              return (
                <Cell
                  title={'Questions & Answers \ud83e\udd14'}
                  cellStyle="Basic"
                  accessory="DisclosureIndicator"
                  onHighlightRow={separators.highlight}
                  onUnHighlightRow={separators.unhighlight}
                  onPress={() => {
                    this.props.navigate('QandA');
                  }}
                />
              );
            case 'licenses':
              return (
                <Cell
                  title={'Licenses \ud83d\udcda '}
                  cellStyle="Basic"
                  accessory="DisclosureIndicator"
                  onHighlightRow={separators.highlight}
                  onUnHighlightRow={separators.unhighlight}
                  onPress={() => {
                    this.props.navigate('Licenses');
                  }}
                />
              );
            case 'signout':
              return (
                <Cell
                  title={'Sign Out \uD83D\uDEAA'}
                  cellStyle="Basic"
                  onHighlightRow={separators.highlight}
                  onUnHighlightRow={separators.unhighlight}
                  onPress={() => {
                    this.props.mutate();
                  }}
                />
              );
          }
        }}
        ItemSeparatorComponent={({ highlighted }) => <Separator isHidden={highlighted} />}
      />
    );
  }
}

SettingsPageBase.propTypes = {
  navigate: PropTypes.func.isRequired,
  mutate: PropTypes.func.isRequired,
};

const LogoutMutation = gql`
  mutation Logout {
    logoutSession {
      id
      status
    }
  }
`;

const SettingsPage = graphql(LogoutMutation)(SettingsPageBase);
export default SettingsPage;
