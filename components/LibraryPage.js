import React from 'react';
import { View, FlatList, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { Cell, Separator } from 'react-native-tableview-simple';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

class LibraryPage extends React.Component {
  static navigationOptions = {
    title: 'Lessons'
  };

  constructor(props) {
    super(props);

    // this.startUpgrade = this.startUpgrade.bind(this);
    this.state = {};
  }

  // async startUpgrade() {
  //   try {
  //     const receipt = await RNIap.buyProduct('course.how_to_everflect');
  //     const variables = Platform.select({
  //       ios: {
  //         receipt: receipt.data,
  //         signature: '',
  //         platform: 'ios'
  //       },
  //       android: {
  //         receipt: receipt.data,
  //         signature: receipt.signature,
  //         platform: 'android'
  //       }
  //     });
  //     this.props
  //       .mutate({
  //         variables
  //       })
  //       .then(results => {
  //         this.props.data.refetch();
  //       });
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  // async componentDidMount() {
  //   // const itemSkus = Platform.select({
  //   //   ios: ['course.how_to_everflect'],
  //   //   android: ['course.how_to_everflect']
  //   // });
  //   try {
  //     // await RNIap.prepare();
  //     // const results = await RNIap.getProducts(itemSkus);
  //     // if (results.length > 0) {
  //     //   // eslint-disable-next-line react/no-did-mount-set-state
  //     //   this.setState({ upgradeRowData: results[0] });
  //     // }
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  render() {
    if (this.props.data.loading) {
      return <ActivityIndicator />;
    }
    let DATA = this.props.data.courseLessons || [];
    return (
      <View style={{ height: '100%', backgroundColor: 'white' }}>
        <FlatList
          refreshing={this.props.data.networkStatus === 4}
          onRefresh={async () => {
            this.props.data.refetch();
            try {
              await RNIap.prepare();
            } catch (e) {
              console.log(e);
            }
          }}
          data={DATA}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, separators }) => {
            // if (item.id === 'upgrade') {
            //   return (
            //     <Cell
            //       title={`Unlock all 10 weeks - only ${
            //         this.state.upgradeRowData.localizedPrice
            //       }`}
            //       cellAccessoryView={<Text>{'\ud83d\ude80'}</Text>}
            //       cellStyle="Basic"
            //       onHighlightRow={separators.highlight}
            //       onUnHighlightRow={separators.unhighlight}
            //       onPress={() => {
            //         this.startUpgrade();
            //       }}
            //     />
            //   );
            // } else {
            return (
              <Cell
                title={item.title}
                cellStyle="Basic"
                onHighlightRow={separators.highlight}
                onUnHighlightRow={separators.unhighlight}
                accessory="DisclosureIndicator"
                isDisabled={!item.lessonAvailable}
                onPress={() => {
                  this.props.navigation.navigate('Lesson', item);
                }}
              />
            );
            // }
          }}
          ItemSeparatorComponent={({ highlighted }) => (
            <Separator isHidden={highlighted} />
          )}
        />
      </View>
    );
  }
}

LibraryPage.propTypes = {
  navigation: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
  mutate: PropTypes.func.isRequired
};

const LibraryQuery = gql`
  query LibraryQuery($courseId: String!) {
    courseLessons(courseId: $courseId) {
      id
      title
      content
      lessonAvailable
    }
  }
`;

const PurchaseMutation = gql`
  mutation ValidatePurchase(
    $receipt: String!
    $signature: String
    $platform: String!
  ) {
    confirmPurchase(
      receipt: $receipt
      platform: $platform
      signature: $signature
    ) {
      id
      status
    }
  }
`;

const LibraryWithData = compose(
  graphql(LibraryQuery, {
    options: {
      variables: {
        courseId: 'C16DB52A-F409-4629-8F0F-D56376C5336C'
      }
    }
  }),
  graphql(PurchaseMutation)
)(LibraryPage);

export default LibraryWithData;
