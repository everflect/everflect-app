import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

import styles from './inventory_steps/styles';

export default class InventoryFoot extends React.Component {
  render() {
    if (this.props.waiting) {
      return (
        <Text style={[styles.text, { textAlign: 'center' }]}>
          ({this.props.name} is ready for the next step)
        </Text>
      );
    } else {
      return null;
    }
  }
}

InventoryFoot.propTypes = {
  waiting: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
};
