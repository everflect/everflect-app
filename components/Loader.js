import React from 'react';
import { Text, View, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

class Loader extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size="large" />
        {this.props.data && this.props.data.me ? (
          <Text>Hi {this.props.data.me.name} - almost ready...</Text>
        ) : (
          <Text>Getting things ready...</Text>
        )}
      </View>
    );
  }
}

Loader.propTypes = {
  data: PropTypes.object.isRequired,
};

const NameQuery = gql`
  query {
    me {
      id
      name
    }
  }
`;
const LoaderWithData = graphql(NameQuery)(Loader);
export default LoaderWithData;
