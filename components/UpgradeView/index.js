import React from 'react';
import { View, Text, Button, ScrollView, Dimensions } from 'react-native';

const Styles = {
  h1: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 24,
  },
  column: {
    flex: 1,
    borderColor: 'black',
    borderWidth: 1,
    padding: 10,
  },
};

const CHECKBOX_ICON = (
  <Text
    style={{
      fontFamily: 'fontawesome',
      fontSize: 12,
    }}>
    {'\uf046' /* check-square-o */}
  </Text>
);

export default class UpgradeView extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Upgrade Now!',
  });

  state = { scrollable: false };

  onLayout(event) {
    const newHeight = event.nativeEvent.layout.height;
    const { height } = Dimensions.get('window');
    if (newHeight > height) {
      this.setState({ scrollable: true });
    } else {
      this.setState({ scrollable: false });
    }
  }

  render() {
    return (
      <ScrollView
        scrollEnabled={this.state.scrollable}
        onContentSizeChange={(_width, height) => {
          const windowHeight = Dimensions.get('window').height * 0.8;
          if (height > windowHeight) {
            this.setState({ scrollable: true });
          } else {
            this.setState({ scrollable: false });
          }
        }}>
        <View style={{ backgroundColor: 'white', flex: 1 }}>
          <View style={{}}>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: 'bold',
                fontSize: 16,
                paddingTop: 25,
                paddingHorizontal: 25,
              }}>
              Upgrade now to unlock more Everflect features!
            </Text>
          </View>
          <View style={{}}>
            {/* I think this section should be a two-column list that compares features, etc.*/}
            <View style={{ flexDirection: 'row', margin: 25 }}>
              <View style={Styles.column}>
                <Text style={Styles.h1}>Everflect Lite</Text>
                <Text>{CHECKBOX_ICON} Basic Questions</Text>
                <Text>{CHECKBOX_ICON} Basic Reminder Notifications</Text>
                <Text>{CHECKBOX_ICON} Two weeks of historical Everflect sessions</Text>
              </View>
              <View style={Styles.column}>
                <Text style={Styles.h1}>Everflect Basic: $9.99</Text>
                <Text>
                  {CHECKBOX_ICON} Access more than 2 weeks of historical Everflect sessions
                </Text>
                <Text>
                  {CHECKBOX_ICON} Full access to "How to Everflect" course, delivered monthly
                </Text>
                <Text>{CHECKBOX_ICON} Support Everflect's continued development</Text>
              </View>
              {/* <View style={Styles.column}>
              <Text style={Styles.h1}>Everflect Premium: $9.99</Text>
              <Text>{CHECKBOX_ICON} Access more than 2 weeks of historical Everflect sessions</Text>
              <Text>
                {CHECKBOX_ICON} Full access to "How to Everflect" course, delivered monthly
              </Text>
              <Text>{CHECKBOX_ICON} Additional "get to know you" questions each session</Text>
              <Text>{CHECKBOX_ICON} Support Everflect's continued development</Text>
            </View> */}
            </View>
          </View>
          <View style={{}}>
            <Button
              style={{}}
              title="Upgrade now"
              accessibilityLabel="Upgrade now"
              onPress={() => {}}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}
