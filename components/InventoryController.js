import React from 'react';
import { View } from 'react-native';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';

import InventoryHead from './InventoryHead';
import InventoryBody from './InventoryBody';
import InventoryFoot from './InventoryFoot';

const viewStyle = {
  flex: 1,
  backgroundColor: '#FFF',
};

class InventoryController extends React.Component {
  static navigationOptions = {
    title: 'Everflect',
  };

  _getWaiting() {
    if (this.props.data.loading || this.props.data.currentInventory === null) {
      return false;
    }
    const stepDifference =
      this.props.data.currentInventory.myStep - this.props.data.currentInventory.spouseStep;
    return (stepDifference < 0);

  }

  render() {
    if (this.props.data) {
      const name = (this.props.data && this.props.data.spouse && this.props.data.spouse.name) ? this.props.data.spouse.name : '';
      return (
        <View style={viewStyle}>
          <InventoryHead data={this.props.data} />
          <InventoryBody
            data={this.props.data}
            endInventoryCb={this.props.screenProps.endInventoryCb}
            waiting={this._getWaiting()}
            spouseName={name}
          />
        </View>
      );
    } else {
      return null;
    }
  }
}

InventoryController.propTypes = {
  data: PropTypes.object.isRequired,
  screenProps: PropTypes.object.isRequired,
};

const InventoryQuery = gql`
  query InventoryController {
    me {
      id
      name
    }
    spouse {
      id
      name
    }
    currentInventory {
      id
      inventoryStatus
      myStep
      spouseStep
    }
    previousInventory {
      id
    }
  }
`;
const InventoryControllerWithData = graphql(InventoryQuery, {
  options: { pollInterval: 1000 },
})(InventoryController);
export default InventoryControllerWithData;
