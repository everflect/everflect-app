import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

export default class EFButton extends React.Component {
    render() {
        return <TouchableOpacity
            style={{
                height: 100,
                justifyContent: 'center',
                backgroundColor: '#90ABDA'
            }}
            onPress={this.props.onPress}
            accessibilityLabel={this.props.label}
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: 'white'
                }}
            >
                {this.props.label}
            </Text>
        </TouchableOpacity>
    }
}