import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    text: {
        fontSize: 20
    },
    stepWrapper: {
        marginHorizontal: 10,
        flexGrow: 9
    },
    flexWrapper: {
        flex: 1,
        display: 'flex'
    }
});

export default styles;
