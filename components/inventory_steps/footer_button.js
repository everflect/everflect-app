import React from 'react';
import {
    Text,
    TouchableOpacity,

} from 'react-native';

import PropTypes from 'prop-types';

export class FooterButton extends React.Component {
    renderToStringWithData() {
        return <TouchableOpacity
            style={{
                flexGrow: 1,
                justifyContent: 'center',
                backgroundColor: '#90ABDA'
            }}
            onPress={this.submitStep}
            accessibilityLabel="Ok, Ready!"
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: 'white'
                }}
            >
                Ok, Ready!
            </Text>
        </TouchableOpacity>
    }
}