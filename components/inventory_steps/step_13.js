import React from 'react';
import { Text, View } from 'react-native';
import * as Segment from 'expo-analytics-segment';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import EFButton from './ef_button';
import InventoryFoot from '../InventoryFoot';
import styles from './styles';

const dateFormat = 'MMMM Do, YYYY [at] h:mm A';

export class Step13 extends React.Component {
  constructor(props) {
    super(props);
    this.submitStep = this.submitStep.bind(this);
    this.trackPermission = this.trackPermission.bind(this);
    this.state = {
      notClicked: true,
      date: moment()
        .add(1, 'days')
        .format(dateFormat),
    };
  }

  trackPermission(type) {
    // Analytics for denied permissions
    Segment.trackWithProperties(type, {
      permission: 'notifications',
      where: 'End of inventory',
    });
  }

  submitStep() {
    if (this.state.notClicked) {
      this.setState({ notClicked: false });
      const inventoryDatetime = moment(this.state.date, dateFormat).format();
      this.props.mutate({
        variables: { inventoryDatetime },
      });
    }
  }

  render() {
    if (this.props.data.loading || this.props.data.currentInventory === null) {
      return <Text style={styles.text}>Loading...</Text>;
    }

    if (!this.props.data.currentInventory.firstSpouseMe) {
      return (
        <Text style={styles.text}>
          Work with {this.props.data.spouse.name} to schedule when you will Everflect next.
        </Text>
      );
    } else {
      return (
        <View style={styles.flexWrapper}>
          <View style={styles.stepWrapper}>
            <Text style={styles.text}>Please schedule when you will Everflect next.</Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: 10,
              }}>
              <DatePicker
                style={{ flex: 1 }}
                date={this.state.date}
                mode="datetime"
                placeholder="select date"
                format={dateFormat}
                minDate={moment()
                  .add(0, 'days')
                  .toDate()}
                maxDate={moment()
                  .add(10, 'days')
                  .toDate()}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    marginLeft: 36,
                  },
                  btnTextCancel: {
                    color: '#000000',
                  },
                  btnTextConfirm: {
                    color: '#000000',
                  },
                }}
                onDateChange={date => {
                  this.setState({ date });
                }}
              />
            </View>
          </View>

          <InventoryFoot waiting={this.props.waiting} name={this.props.spouseName} />
          <EFButton onPress={this.submitStep} label="Ok, Ready!" />
        </View>
      );
    }
  }
}

Step13.propTypes = {
  mutate: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  waiting: PropTypes.bool.isRequired,
  spouseName: PropTypes.string,
};

const NameQuery = gql`
  query {
    me {
      id
      name
    }
    spouse {
      id
      name
    }
    currentInventory {
      id
      firstSpouseMe
    }
  }
`;
const StepMutation = gql`
  mutation ScheduleEverflect($inventoryDatetime: String!) {
    scheduleEverflect(inventoryDatetime: $inventoryDatetime) {
      success
    }
  }
`;
const Step13WithData = compose(
  graphql(NameQuery, {}),
  graphql(StepMutation, {})
)(Step13);
export default Step13WithData;
