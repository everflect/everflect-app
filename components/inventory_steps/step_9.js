import React from 'react';
import { Text, View, TextInput, Button } from 'react-native';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import styles from './styles';

export class Step9 extends React.Component {
  constructor(props) {
    super(props);
    this.submitStep = this.submitStep.bind(this);
    this.advanceField1 = this.advanceField1.bind(this);
    this.advanceField2 = this.advanceField2.bind(this);
    this.advanceField3 = this.advanceField3.bind(this);

    this.state = { notClicked: true, praise1: '', praise2: '', praise3: '' };
  }

  submitStep() {
    if (
      this.state.notClicked &&
      this.state.praise1.length > 0 &&
      this.state.praise2.length > 0 &&
      this.state.praise3.length > 0
    ) {
      this.setState({
        notClicked: false,
        praise1Error: null,
        praise2Error: null,
        praise3Error: null,
      });
      this.props.mutate({
        variables: {
          praise1: this.state.praise1,
          praise2: this.state.praise2,
          praise3: this.state.praise3,
        },
      });
    }
  }

  advanceField1() {
    if (this.state.praise1.length > 0) {
      this.setState({ praise1Error: null });
      this._praise2box.focus();
    } else {
      this.setState({
        praise1Error: `Enter your praise for ${this.props.data.spouse.name} first`,
      });
    }
  }

  advanceField2() {
    if (this.state.praise2.length > 0) {
      this.setState({ praise2Error: null });
      this._praise3box.focus();
    } else {
      this.setState({
        praise2Error: `Enter your praise for ${this.props.data.spouse.name} first`,
      });
    }
  }

  advanceField3() {
    if (this.state.praise3.length > 0) {
      this.setState({ praise3Error: null });
      this.submitStep();
    } else {
      this.setState({
        praise3Error: `Enter your praise for ${this.props.data.spouse.name} first`,
      });
    }
  }

  render() {
    if (this.props.data.loading) {
      return <Text>Loading...</Text>;
    }
    if (!this.props.data.currentInventory.firstSpouseMe) {
      return (
        <View style={styles.stepWrapper}>
          <Text style={styles.text}>
            Please share three specific things you have appreciated about{' '}
            {this.props.data.spouse.name} this past week.
          </Text>
        </View>
      );
    } else {
      return (
        <View style={styles.stepWrapper}>
          <Text style={styles.text}>
            Please record the three things {this.props.data.spouse.name} appreciated about you this
            past week:
          </Text>
          <TextInput
            ref={c => (this._praise1box = c)}
            style={{
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              paddingHorizontal: 10,
            }}
            onChangeText={praise1 => this.setState({ praise1 })}
            onSubmitEditing={this.advanceField1}
            value={this.state.praise1}
            placeholder="e.g. You helped me with my goals"
            enablesReturnKeyAutomatically
            autoFocus
            blurOnSubmit={false}
          />
          {this.state.praise1Error ? <Text style={styles.text}>{this.state.praise1Error}</Text> : null}
          <TextInput
            ref={c => (this._praise2box = c)}
            style={{
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              paddingHorizontal: 10,
            }}
            onChangeText={praise2 => this.setState({ praise2 })}
            onSubmitEditing={this.advanceField2}
            value={this.state.praise2}
            placeholder="e.g. You were patient when I was tired"
            enablesReturnKeyAutomatically
            blurOnSubmit={false}
          />
          {this.state.praise2Error ? <Text style={styles.text}>{this.state.praise2Error}</Text> : null}
          <TextInput
            ref={c => (this._praise3box = c)}
            style={{
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              paddingHorizontal: 10,
            }}
            onChangeText={praise3 => this.setState({ praise3 })}
            onSubmitEditing={this.advanceField3}
            value={this.state.praise3}
            placeholder="e.g. You listened when I needed to talk"
            enablesReturnKeyAutomatically
            blurOnSubmit={false}
          />
          {this.state.praise3Error ? <Text style={styles.text}>{this.state.praise3Error}</Text> : null}
        </View>
      );
    }
  }
}

Step9.propTypes = {
  mutate: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

const NameQuery = gql`
  query {
    me {
      id
      name
    }
    spouse {
      id
      name
    }
    currentInventory {
      id
      firstSpouseMe
    }
  }
`;
const StepMutation = gql`
  mutation SetSpousePraise($praise1: String!, $praise2: String!, $praise3: String!) {
    setSpousePraise(praise1: $praise1, praise2: $praise2, praise3: $praise3) {
      success
    }
  }
`;
const Step9WithData = compose(graphql(NameQuery, {}), graphql(StepMutation, {}))(Step9);
export default Step9WithData;
