import React from 'react';
import {
  Text,
  View,
  Dimensions,
  ScrollView
} from 'react-native';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import InventoryPartial from '../InventoryPartial';

import EFButton from './ef_button';
import InventoryFoot from '../InventoryFoot';
import styles from './styles';

export class Step1 extends React.Component {
  constructor(props) {
    super(props);
    this.submitStep = this.submitStep.bind(this);
    this.state = { notClicked: true, scrollable: false };
  }

  onLayout(event) {
    const newHeight = event.nativeEvent.layout.height;
    const { height } = Dimensions.get('window');
    if (newHeight > height) {
      this.setState({ scrollable: true });
    } else {
      this.setState({ scrollable: false });
    }
  }

  submitStep() {
    if (this.state.notClicked) {
      this.setState({ notClicked: false });
      this.props.mutate();
    }
  }

  render() {
    if (this.props.data.loading) {
      return <Text>Loading...</Text>;
    }
    if (this.props.data.previousInventory === null) {
      return <Text>Loading...</Text>;
    }
    return (
      <View style={styles.flexWrapper}>
        <ScrollView
          scrollEnabled={this.state.scrollable}
          onContentSizeChange={(_width, height) => {
            const windowHeight = Dimensions.get('window').height * 0.8;
            if (height > windowHeight) {
              this.setState({ scrollable: true });
            } else {
              this.setState({ scrollable: false });
            }
          }}
        >
          <View style={styles.stepWrapper}>
            <Text style={styles.text}>
              Before we begin, let's review your goals from last time
            </Text>
            <InventoryPartial
              inventoryId={this.props.data.previousInventory.id}
            />
          </View>
        </ScrollView>

        <InventoryFoot waiting={this.props.waiting} name={this.props.spouseName} />
        <EFButton onPress={this.submitStep} label="Ok, Ready!" />
      </View>
    );
  }
}

Step1.propTypes = {
  mutate: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  waiting: PropTypes.bool.isRequired,
  spouseName: PropTypes.string,
};

const NameQuery = gql`
  query Step1Query {
    me {
      id
      name
    }
    previousInventory {
      id
    }
  }
`;
const StepMutation = gql`
  mutation AdvanceStep {
    advanceSpouseStep {
      success
    }
  }
`;
const Step1WithData = compose(
  graphql(NameQuery, {}),
  graphql(StepMutation, {})
)(Step1);
export default Step1WithData;
