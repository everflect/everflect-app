import Inventory_Step1 from './step_1';
import Inventory_Step2 from './step_2';
import Inventory_Step3 from './step_3';
import Inventory_Step4 from './step_4';
import Inventory_Step5 from './step_5';
import Inventory_Step6 from './step_6';
import Inventory_Step7 from './step_7';
import Inventory_Step8 from './step_8';
import Inventory_Step9 from './step_9';
import Inventory_Step10 from './step_10';
import Inventory_Step11 from './step_11';
import Inventory_Step12 from './step_12';
import Inventory_Step13 from './step_13';

import StepWaiting from './step_waiting';

export default {
  StepWaiting,
  Inventory_Step1,
  Inventory_Step2,
  Inventory_Step3,
  Inventory_Step4,
  Inventory_Step5,
  Inventory_Step6,
  Inventory_Step7,
  Inventory_Step8,
  Inventory_Step9,
  Inventory_Step10,
  Inventory_Step11,
  Inventory_Step12,
  Inventory_Step13,
};

export { StepWaiting };
export { Inventory_Step1 };
export { Inventory_Step2 };
export { Inventory_Step3 };
export { Inventory_Step4 };
export { Inventory_Step5 };
export { Inventory_Step6 };
export { Inventory_Step7 };
export { Inventory_Step8 };
export { Inventory_Step9 };
export { Inventory_Step10 };
export { Inventory_Step11 };
export { Inventory_Step12 };
export { Inventory_Step13 };
