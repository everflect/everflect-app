import React from 'react';
import { Text, View, TextInput } from 'react-native';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import styles from './styles';

export class Step10 extends React.Component {
  constructor(props) {
    super(props);
    this.submitStep = this.submitStep.bind(this);
    this.state = { notClicked: true, improvement: '' };
  }

  submitStep() {
    if (this.state.notClicked && this.state.improvement.length > 0) {
      this.setState({ notClicked: false });
      this.props.mutate({
        variables: { improvement: this.state.improvement },
      });
    }
  }

  render() {
    // Spouse A === First Spouse:
    // Please ask Michelle for one idea on how you can improve this week. [record here]
    // Spouse B !== First Spouse
    // When Brent asks, please share one idea on how Brent can improve this week.
    if (this.props.data.loading) {
      return <Text style={styles.text}>Loading...</Text>;
    }
    if (this.props.data.currentInventory.firstSpouseMe) {
      return (
        <View style={styles.stepWrapper}>
          <Text style={styles.text}>
            When {this.props.data.spouse.name} asks, please share one idea on how{' '}
            {this.props.data.spouse.name} can improve this week.
          </Text>
        </View>
      );
    } else {
      return (
        <View style={styles.stepWrapper}>
          <Text style={styles.text}>
            Please ask {this.props.data.spouse.name} for one idea on how you can improve this week.
          </Text>
          <TextInput
            style={{
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              paddingHorizontal: 10,
            }}
            onChangeText={improvement => this.setState({ improvement })}
            onSubmitEditing={this.submitStep}
            value={this.state.goal}
            placeholder={`${this.props.data.spouse.name} would appreciate it if you...`}
            enablesReturnKeyAutomatically
            autoFocus
            blurOnSubmit={false}
          />
          {this.state.improvementError ? <Text style={styles.text}>{this.state.improvementError}</Text> : null}
        </View>
      );
    }
  }
}

Step10.propTypes = {
  mutate: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

const NameQuery = gql`
  query {
    me {
      id
      name
    }
    spouse {
      id
      name
    }
    currentInventory {
      id
      firstSpouseMe
    }
  }
`;
const StepMutation = gql`
  mutation SetSpouseImprovement($improvement: String!) {
    setSpouseImprovement(improvement: $improvement) {
      success
    }
  }
`;
const Step10WithData = compose(graphql(NameQuery, {}), graphql(StepMutation, {}))(Step10);
export default Step10WithData;
