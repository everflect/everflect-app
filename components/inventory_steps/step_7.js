import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import EFButton from './ef_button';
import InventoryFoot from '../InventoryFoot';
import styles from './styles';

export class Step7 extends React.Component {
  constructor(props) {
    super(props);
    this.submitStep = this.submitStep.bind(this);
    this.state = { notClicked: true };
  }

  submitStep() {
    if (this.state.notClicked) {
      this.setState({ notClicked: false });
      this.props.mutate();
    }
  }

  render() {
    return (
      <View style={styles.flexWrapper}>
        <View style={styles.stepWrapper}>
          <Text style={styles.text}>Resolve any conflict you may have.</Text>
        </View>

        <InventoryFoot waiting={this.props.waiting} name={this.props.spouseName} />
        <EFButton onPress={this.submitStep} label="Ok, Ready!" />
      </View>
    );
  }
}

Step7.propTypes = {
  mutate: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  waiting: PropTypes.bool.isRequired,
  spouseName: PropTypes.string,
};

const NameQuery = gql`
  query {
    me {
      id
      name
    }
  }
`;
const StepMutation = gql`
  mutation AdvanceStep {
    advanceSpouseStep {
      success
    }
  }
`;
const Step7WithData = compose(graphql(NameQuery, {}), graphql(StepMutation, {}))(Step7);
export default Step7WithData;
