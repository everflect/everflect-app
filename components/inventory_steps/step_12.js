import React from 'react';
import { Text, View, Button, ScrollView, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import InventoryPartial from '../InventoryPartial';
import EFButton from './ef_button';
import InventoryFoot from '../InventoryFoot';
import styles from './styles';

export class Step12 extends React.Component {
  constructor(props) {
    super(props);
    this.submitStep = this.submitStep.bind(this);
    this.state = { notClicked: true };
  }

  submitStep() {
    if (this.state.notClicked) {
      this.setState({ notClicked: false });
      this.props.mutate();
    }
  }

  render() {
    if (this.props.data.loading) {
      return <Text style={styles.text}>Loading...</Text>;
    }
    return (
      <View style={styles.flexWrapper}>
        <ScrollView
          scrollEnabled={this.state.scrollable}
          onContentSizeChange={(_width, height) => {
            const windowHeight = Dimensions.get('window').height * 0.8;
            if (height > windowHeight) {
              this.setState({ scrollable: true });
            } else {
              this.setState({ scrollable: false });
            }
          }}>
          <View style={[{ height: '100%' }, styles.stepWrapper]}>
            <Text style={styles.text}>Let's review this week's goals:</Text>
            <InventoryPartial inventoryId={this.props.data.currentInventory.id} />
          </View>
        </ScrollView>

        <InventoryFoot waiting={this.props.waiting} name={this.props.spouseName} />
        <EFButton onPress={this.submitStep} label="Ok, Ready!" />
      </View>
    );
  }
}

Step12.propTypes = {
  mutate: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  waiting: PropTypes.bool.isRequired,
  spouseName: PropTypes.string,
};

const NameQuery = gql`
  query {
    me {
      id
      name
    }
    spouse {
      id
      name
    }
    currentInventory {
      id
      myGoal
      myImprovement
      myGoalSupport
      myPraise1
      myPraise2
      myPraise3
      spouseGoal
      spouseImprovement
      spouseGoalSupport
      spousePraise1
      spousePraise2
      spousePraise3
    }
  }
`;
const StepMutation = gql`
  mutation AdvanceStep {
    advanceSpouseStep {
      success
    }
  }
`;
const Step12WithData = compose(graphql(NameQuery, {}), graphql(StepMutation, {}))(Step12);
export default Step12WithData;
