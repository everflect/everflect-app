import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import EFButton from './ef_button';

class StepWaitingBasic extends React.Component {
  constructor(props) {
    super(props);
    this.submitStep = this.submitStep.bind(this);
    this.state = { notClicked: true };
  }

  submitStep() {
    if (this.state.notClicked) {
      this.setState({ notClicked: false });
      this.props.mutate();
    }
  }

  render() {
    return (
      <View style={{ flex: 1, display: 'flex' }}>
        <View style={{ marginHorizontal: 10, flexGrow: 9 }}>
          <Text style={{ fontSize: 20 }}>Waiting for {this.props.data.spouse.name || ' your spouse'}...</Text>
        </View>
        <EFButton label="Go back" onPress={this.submitStep} />
      </View>
    );
  }
}

StepWaitingBasic.propTypes = {
  mutate: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

const NameQuery = gql`
  query {
    spouse {
      id
      name
    }
    currentInventory {
      id
      myStep
    }
  }
`;
const StepMutation = gql`
  mutation RollbackStep {
    rollbackSpouseStep {
      success
    }
  }
`;
const StepWaiting = compose(graphql(NameQuery), graphql(StepMutation))(StepWaitingBasic);
export default StepWaiting;
