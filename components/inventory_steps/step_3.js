import React from 'react';
import { Text, View, TextInput, KeyboardAvoidingView, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import styles from './styles';

export class Step3 extends React.Component {
  constructor(props) {
    super(props);
    this.submitGoal = this.submitGoal.bind(this);
    this.submitHelp = this.submitHelp.bind(this);
    this.state = { notClicked: true, goal: '', help: '' };
  }

  submitGoal() {
    if (this.state.goal.length > 0) {
      this._helpInput.focus();
    } else {
      this.setState({
        goalError: `Enter ${this.props.data.spouse.name}'s goal first`,
      });
    }
  }

  submitHelp() {
    if (this.state.help.length === 0) {
      this.setState({
        helpError: `Enter how you can help ${this.props.data.spouse.name} with their goal first`,
      });
      return;
    }
    if (this.state.notClicked) {
      this.setState({ notClicked: false });
      this.props.mutate({
        variables: {
          goal: this.state.goal,
          help: this.state.help,
        },
      });
    }
  }

  render() {
    // Spouse A === First Spouse:
    // Please share your perosnal goal with Michelle and ask for help in achieving it.
    // Spouse B === First Spouse
    // What is Brent's goal for the upcoming week [record here]. How can you help Brent achieve this goal? [record here]
    if (this.props.data.loading) {
      return <Text>Loading...</Text>;
    }
    if (this.props.data.currentInventory.firstSpouseMe) {
      return (
        <View style={styles.stepWrapper}>
          <Text style={[styles.text, { textAlign: 'center' }]}>
            Please share your personal goal with {this.props.data.spouse.name} and ask for help in
            achieving it.
          </Text>
        </View>
      );
    } else {
      const { height } = Dimensions.get('window');
      const h = -1 * height;
      return (
        <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={h} style={[styles.flexWrapper]}>
          <View style={styles.stepWrapper}>
            <Text style={styles.text}>What is {this.props.data.spouse.name}'s goal for the upcoming week?</Text>
            <TextInput
              style={{
                height: 40,
                borderColor: 'gray',
                borderWidth: 1,
                paddingHorizontal: 10,
              }}
              onChangeText={goal => this.setState({ goal })}
              onSubmitEditing={this.submitGoal}
              value={this.state.goal}
              placeholder="e.g. Go running every morning"
              enablesReturnKeyAutomatically
              autoFocus
              blurOnSubmit={false}
            />
            {this.state.goalError ? <Text>{this.state.goalError}</Text> : null}
            <Text style={styles.text}>How can you help {this.props.data.spouse.name} achieve this goal?</Text>
            <TextInput
              ref={c => (this._helpInput = c)}
              style={{
                height: 40,
                borderColor: 'gray',
                borderWidth: 1,
                paddingHorizontal: 10,
              }}
              onChangeText={help => this.setState({ help })}
              onSubmitEditing={this.submitHelp}
              value={this.state.help}
              placeholder={`e.g. Go running with ${this.props.data.spouse.name}`}
              enablesReturnKeyAutomatically
            />
            {this.state.helpError ? <Text>{this.state.helpError}</Text> : null}
          </View>
        </KeyboardAvoidingView>
      );
    }
  }
}

Step3.propTypes = {
  mutate: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  waiting: PropTypes.bool.isRequired,
  spouseName: PropTypes.string,
};

const NameQuery = gql`
  query {
    me {
      id
      name
    }
    spouse {
      id
      name
    }
    currentInventory {
      id
      firstSpouseMe
    }
  }
`;
const StepMutation = gql`
  mutation SetSpouseGoals($goal: String!, $help: String!) {
    setSpouseGoals(goal: $goal, help: $help) {
      success
    }
  }
`;
const Step3WithData = compose(graphql(NameQuery, {}), graphql(StepMutation, {}))(Step3);
export default Step3WithData;
