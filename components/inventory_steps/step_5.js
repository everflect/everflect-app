import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import EFButton from './ef_button';
import InventoryFoot from '../InventoryFoot';
import styles from './styles';

export class Step5 extends React.Component {
  constructor(props) {
    super(props);
    this.submitStep = this.submitStep.bind(this);
    this.state = { notClicked: true };
  }

  submitStep() {
    if (this.state.notClicked) {
      this.setState({ notClicked: false });
      this.props.mutate();
    }
  }

  render() {
    return (
      <View style={styles.flexWrapper}>
        <View style={styles.stepWrapper}>
          <Text style={styles.text}>
            Great! Now please take a few minutes to discuss the strength of your relationship with{' '}
            {this.props.data.spouse.name}.
        </Text>
        </View>

        <InventoryFoot waiting={this.props.waiting} name={this.props.spouseName} />
        <EFButton onPress={this.submitStep} label="Ok, Ready!" />
      </View>
    );
  }
}

Step5.propTypes = {
  mutate: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  waiting: PropTypes.bool.isRequired,
  spouseName: PropTypes.string,
};

const NameQuery = gql`
  query {
    me {
      id
      name
    }
    spouse {
      id
      name
    }
  }
`;
const StepMutation = gql`
  mutation AdvanceStep {
    advanceSpouseStep {
      success
    }
  }
`;
const Step5WithData = compose(graphql(NameQuery, {}), graphql(StepMutation, {}))(Step5);
export default Step5WithData;
