<View
  style={{
    marginHorizontal: 10,
    flex: 1,
    display: 'flex',
    height: '100%',
    // flexDirection: 'column',
    // justifyContent: 'flex-end',
    // alignItems: 'center',
  }}>
  <Text style={{ textAlign: 'center' }}>
    Before we begin, let's review your goals from last time
  </Text>
  <Text style={{ flexGrow: '1', textAlign: 'center' }}>FooBar</Text>
  <Button
    style={{ alignSelf: 'flex-end', backgroundColor: 'blue' }}
    onPress={this.submitStep}
    title="Ok, Ready!"
    accessibilityLabel="Ok, Ready!"
  />
</View>;
