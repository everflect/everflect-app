/**
 * Copyright (c) 2015 Loch Wansbrough
 *
 * Lifted from https://github.com/lwansbrough/react-native-progress-bar on Nov 6, 2017
 */

import React from 'react';
import { StyleSheet, View, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types';

var styles = StyleSheet.create({
  background: {
    backgroundColor: '#bbbbbb',
    height: 5,
    overflow: 'hidden',
  },
  fill: {
    backgroundColor: '#3b5998',
    height: 5,
  },
});

export default class ProgressBar extends React.Component {
  static defaultProps = {
    style: styles,
    easing: Easing.inOut(Easing.ease),
    easingDuration: 500,
  };

  constructor(props) {
    super(props);
    this.state = {
      progress: new Animated.Value(this.props.initialProgress || 0),
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.progress >= 0 && this.props.progress !== prevProps.progress) {
      this.update();
    }
  }

  componentDidMount() {
    this.update();
  }

  render() {
    var fillWidth = this.state.progress.interpolate({
      inputRange: [0, 1],
      outputRange: [0 * this.props.style.width, 1 * this.props.style.width],
    });

    return (
      <View style={[styles.background, this.props.backgroundStyle, this.props.style]}>
        <Animated.View style={[styles.fill, this.props.fillStyle, { width: fillWidth }]} />
      </View>
    );
  }

  update() {
    Animated.timing(this.state.progress, {
      easing: this.props.easing,
      duration: this.props.easingDuration,
      toValue: this.props.progress,
    }).start();
  }
}

ProgressBar.propTypes = {
  easing: PropTypes.func,
  easingDuration: PropTypes.number,
  progress: PropTypes.number,
  fillStyle: PropTypes.object,
  backgroundStyle: PropTypes.object,
  initialProgress: PropTypes.number,
  style: PropTypes.object,
};
