import React from 'react';
import { View, AsyncStorage } from 'react-native';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';

import NamePromptContainer from './NamePrompt';
import JoinTokenPromptWithMutation from './JoinTokenPrompt';
import PostJoin from './PostJoin';
import Loader from '../Loader';
import NotificationPermissions from './NotificationPermissions';

class OnboardingController extends React.Component {
  static navigationOptions = {
    title: 'Welcome',
  };

  constructor(props) {
    super(props);
    this.state = {
      confirmedJoin: false,
      stepIndex: 0,
      notificationsRequested: false,
    };
    this.reloadSection = this.reloadSection.bind(this);
    this.confirmedJoinCb = this.confirmedJoinCb.bind(this);
    this.advanceStep = this.advanceStep.bind(this);
  }

  componentWillReceiveProps(props) {
    AsyncStorage.getItem('@EF:PermissionsRequested.Notifications').then(requested => {
      this.setState({ notificationsRequested: requested });
    });
  }

  advanceStep() {
    this.setState({ stepIndex: this.state.stepIndex + 1 });
  }

  reloadSection() {
    this.props.data.refetch();
  }

  confirmedJoinCb() {
    if (this.state.notificationsRequested) {
      this.props.onboardingComplete();
    }
    this.setState({ confirmedJoin: true });
  }

  render() {
    let NEXT_WIDGET;
    if (this.props.data.loading) {
      NEXT_WIDGET = <Loader />;
    } else if (!this.props.data.me || !this.props.data.me.name) {
      NEXT_WIDGET = <NamePromptContainer onSubmit={this.reloadSection} />;
    } else if (this.props.data.join && this.props.data.join.token) {
      NEXT_WIDGET = <JoinTokenPromptWithMutation onSubmit={this.reloadSection} />;
    } else if (this.props.data.couple && this.state.confirmedJoin === false) {
      NEXT_WIDGET = <PostJoin onSubmit={this.confirmedJoinCb} />;
    } else if (this.state.confirmedJoin === true) {
      NEXT_WIDGET = <NotificationPermissions onSubmit={this.props.onboardingComplete} />;
    }

    return <View>{NEXT_WIDGET}</View>;
  }
}

OnboardingController.propTypes = {
  data: PropTypes.object.isRequired,
  onboardingComplete: PropTypes.func.isRequired,
};

const MyNameQuery = gql`
  query {
    me {
      id
      name
      email
    }
    couple {
      id
      joinedCourse
    }
    join {
      token
    }
  }
`;
const OnboardingControllerWithData = graphql(MyNameQuery)(OnboardingController);

export default OnboardingControllerWithData;
