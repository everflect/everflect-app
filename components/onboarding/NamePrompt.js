import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, Button } from 'react-native';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import t from 'tcomb-form-native';

var Form = t.form.Form;

var Person = t.struct({
  name: t.String,
});

class NamePrompt extends React.Component {
  constructor(props) {
    super(props);

    this.submitForm = this.submitForm.bind(this);

    this.formOptions = {
      fields: {
        name: {
          label: 'What is your first name?',
          placeholder: 'e.g. Taylor',
          autoFocus: true,
          error: 'Please enter your first name',
          enablesReturnKeyAutomatically: true,
          onBlur: () => {
            this.submitForm();
          },
        },
      },
    };
  }

  submitForm() {
    const val = this._form.getValue();
    if (val) {
      this.props
        .mutate({ variables: val })
        .then(() => {
          this.props.onSubmit();
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  render() {
    return (
      <View style={{ margin: 10 }}>
        <Text
          style={{
            fontFamily: 'fontawesome',
            fontSize: 48,
            textAlign: 'center',
            marginBottom: 30,
          }}>
          {'\uf118' /* smile-o */}
        </Text>

        <Form
          ref={form => {
            this._form = form;
          }}
          type={Person}
          options={this.formOptions}
        />
        {/*TODO - put in an arrow on the button title: Continue \uf061*/}
        <Button onPress={this.submitForm} title="Continue" />
      </View>
    );
  }
}

NamePrompt.propTypes = {
  mutate: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

const NameMutation = gql`
  mutation setupSpouse($name: String!) {
    setMyName(name: $name) {
      id
      name
      email
    }
  }
`;
const NamePromptWithMutation = graphql(NameMutation)(NamePrompt);
export default NamePromptWithMutation;
