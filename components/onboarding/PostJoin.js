import React from 'react';
import { Button, View, Text } from 'react-native';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';

import LoaderWithData from '../Loader';

// Root class for the app interface
class PostJoin extends React.Component {
  render() {
    if (this.props.data.loading === true || this.props.data.spouse === null) {
      return (
        <View style={{ height: '100%' }}>
          <LoaderWithData />
        </View>
      );
    } else {
      return (
        <View style={{ height: '100%', margin: 20 }}>
          <Text
            style={{
              fontFamily: 'fontawesome',
              fontSize: 48,
              textAlign: 'center',
              marginTop: 24,
              marginBottom: 24,
            }}>
            {'\uf0c1' /* fa-link */}
          </Text>
          <Text style={{ fontSize: 18, textAlign: 'center' }}>
            Great! You are now connected to {this.props.data.spouse.name}!
          </Text>
          <Button title="Next" onPress={this.props.onSubmit} />
        </View>
      );
    }
  }
}

PostJoin.propTypes = {
  data: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

const MyNameQuery = gql`
  query {
    me {
      id
      name
    }
    spouse {
      id
      name
    }
    currentInventory {
      id
      inventoryStatus
    }
  }
`;

const PostJoinWithData = graphql(MyNameQuery, {
  options: { pollInterval: 1000 },
})(PostJoin);

export default PostJoinWithData;
