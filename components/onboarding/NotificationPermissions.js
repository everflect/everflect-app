import React from 'react';
import { Button, View, Text, AsyncStorage } from 'react-native';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import * as Segment from 'expo-analytics-segment';
import * as Permissions from 'expo-permissions';

import PropTypes from 'prop-types';

class NotificationPermissions extends React.Component {
  render() {
    return (
      <View style={{ margin: 10 }}>
        <Text>
          During your week, Everflect will help remind you and {this.props.data.spouse.name} about
          your goals.
        </Text>
        <Button
          title="Not now"
          onPress={() => {
            this.props.onSubmit();
          }}
        />
        <Button
          title="Allow Notifications"
          onPress={() => {
            Permissions.askAsync(Permissions.NOTIFICATIONS).then(({ status }) => {
              if (status !== 'granted') {
                Segment.trackWithProperties('DeniedPermissions', {
                  permission: 'notifications',
                  where: 'End of inventory',
                });
              }
              AsyncStorage.setItem('@EF:PermissionsRequested.Notifications', 'true').then(
                this.props.onSubmit
              );
            });
          }}
        />
      </View>
    );
  }
}

NotificationPermissions.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

const MyNameQuery = gql`
  query {
    spouse {
      id
      name
    }
  }
`;

const NotificationPermissionsWithData = graphql(MyNameQuery)(NotificationPermissions);

export default NotificationPermissionsWithData;
