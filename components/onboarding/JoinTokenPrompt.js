import React from 'react';
import {
  Text,
  View,
  Button,
  Share,
  Alert,
  KeyboardAvoidingView,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import t from 'tcomb-form-native';
import * as Sentry from 'sentry-expo';

var Form = t.form.Form;

const TokenType = t.refinement(t.String, token => {
  const re = /^[a-zA-Z0-9]{5}$/;
  return re.test(token);
});

const JoinTokenType = t.struct({
  token: TokenType
});

class JoinTokenPrompt extends React.Component {
  constructor(props) {
    super(props);
    this.submitJoinToken = this.submitJoinToken.bind(this);
    this.shareSheet = this.shareSheet.bind(this);

    this.formOptions = {
      fields: {
        token: {
          label: ' ',
          placeholder: 'e.g. A1B2C',
          maxLength: 5,
          autoFocus: true,
          enablesReturnKeyAutomatically: true,
          onSubmitEditing: () => {
            this.submitJoinToken();
          }
        }
      }
    };
  }

  shareSheet() {
    const url =
      'https://itunes.apple.com/us/app/everflect/id1333393241?ls=1&mt=8';
    Share.share({
      title: 'Share with your spouse',
      message: `Let's try using Everflect to improve our relationship! ${url}`,
      url
    });
  }

  submitJoinToken() {
    const val = this._form.getValue();
    if (val) {
      this.props
        .mutate({
          variables: { token: val.token }
        })
        .then(({ data }) => {
          this.props.onSubmit();
        })
        .catch(e => {
          Sentry.captureException(e);
          Alert.alert(
            'Oops!',
            'Something went wrong connecting to our servers. Please try again later.',
            [{ text: 'Okay', onPress: () => {} }]
          );
        });
    }
  }

  render() {
    // When we re-render and have no join-token, it means our spouse joined
    if (this.props.data.join.token === null) {
      this.props.onSubmit();
    }

    const { height } = Dimensions.get('window');
    // Inverse relationship between screen height and keyboard vertical offset
    const h = 40000 / height;

    return (
      <View style={{ margin: 10 }}>
        <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={h}>
          <Text
            style={{
              fontFamily: 'fontawesome',
              fontSize: 48,
              textAlign: 'center',
              marginBottom: 30
            }}
          >
            {'\uf2be' /* user-circle-o */}
          </Text>
          <Text style={{ fontSize: 15 }}>
            Hi {this.props.data.me.name}, it&rsquo;s nice to meet you.
          </Text>
          <Text style={{ fontSize: 15 }}>
            To connect with your spouse, please share this code with them:
          </Text>
          <Text
            style={{
              marginVertical: 10,
              textAlign: 'center',
              fontSize: 20,
              fontFamily: 'courierBold'
            }}
          >
            {this.props.data.join.token}
          </Text>
          <Text style={{ fontSize: 15 }}>Or, enter their code below</Text>
          <Form
            ref={form => {
              this._form = form;
            }}
            type={JoinTokenType}
            options={this.formOptions}
          />
          <Button onPress={this.submitJoinToken} title="Connect" />
          <Button
            onPress={this.shareSheet}
            title="Share Everflect with your spouse"
          />
        </KeyboardAvoidingView>
      </View>
    );
  }
}

JoinTokenPrompt.propTypes = {
  mutate: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired
};

const JoinTokenMutation = gql`
  mutation UseJoinToken($token: String!) {
    applyJoinToken(token: $token) {
      id
    }
  }
`;
const JoinTokenQuery = gql`
  query {
    me {
      id
      name
    }
    join {
      token
    }
  }
`;

const JoinTokenPromptWithMutation = compose(
  graphql(JoinTokenMutation, {}),
  graphql(JoinTokenQuery, { options: { pollInterval: 500 } })
)(JoinTokenPrompt);

export default JoinTokenPromptWithMutation;
