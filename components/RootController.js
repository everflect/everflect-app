import React from 'react';
import PropTypes from 'prop-types';
import { View, NetInfo, Text, ActivityIndicator } from 'react-native';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import AuthenticatedRootController from './AuthenticatedRootController';
import { IntroController } from './login';

// Root class for the app interface
export class RootController extends React.Component {
  constructor(props) {
    super(props);

    this.state = { isConnected: true, delay: true };

    this._handleConnectionChange = this._handleConnectionChange.bind(this);
    NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
  }

  _handleConnectionChange(isConnected) {
    this.setState({ isConnected });
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ delay: false });
    }, 1000);
  }

  render() {
    if (this.state.isConnected) {
      if (this.props.data.loading || this.state.delay) {
        return (
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#90ABDA',
            }}>
            <ActivityIndicator color="white" size="large" />
          </View>
        );
      } else {
        return (
          <View
            style={{
              height: '100%',
            }}>
            {this.props.data.deviceAuthentication.loggedIn ? (
              <AuthenticatedRootController />
            ) : (
              <IntroController visible={!this.props.data.deviceAuthentication.loggedIn} />
            )}
          </View>
        );
      }
    } else {
      return (
        <View
          style={{
            height: '100%',
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: 'fontawesome',
              fontSize: 48,
              textAlign: 'center',
              marginBottom: 10,
            }}>
            {'\uf119' /* frown-o */}
          </Text>
          <Text>Everflect requires an internet connection - please connect to the internet</Text>
        </View>
      );
    }
  }
}

RootController.propTypes = {
  data: PropTypes.object.isRequired,
};

const RootQuery = gql`
  query RootController {
    deviceAuthentication @client {
      loggedIn
    }
  }
`;

const RootControllerWithData = graphql(RootQuery)(RootController);

export default RootControllerWithData;
