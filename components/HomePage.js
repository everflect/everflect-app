import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import InventoryList from './InventoryList';
import InventoryModal from './InventoryModal';

// Root class for the app interface
class HomePage extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Inventories',
    };
  };

  constructor(props) {
    super(props);

    this.startInventoryCb = this.startInventoryCb.bind(this);
    this.endInventoryCb = this.endInventoryCb.bind(this);
    this.state = { inventoryRunning: false };
  }

  componentWillMount() {
    this.props.navigation.setParams({
      showInventory: false,
      inventoryWaiting: true,
    });
  }

  startInventoryCb() {
    this.props.mutate().then(_results => {
      setTimeout(() => {
        this.setState({ inventoryRunning: true });
      });
    });
  }

  endInventoryCb() {
    this.setState({ inventoryRunning: false });
  }

  render() {
    return (
      <View style={{ height: '100%' }}>
        <InventoryModal
          visible={this.state.inventoryRunning}
          endInventoryCb={this.endInventoryCb}
        />
        <InventoryList startInventoryCb={this.startInventoryCb} />
      </View>
    );
  }
}

HomePage.propTypes = {
  mutate: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

const StartInventoryMutation = gql`
  mutation StartInventory {
    startInventory {
      id
    }
  }
`;

const HomePageWithMutation = graphql(StartInventoryMutation)(HomePage);
export default HomePageWithMutation;
