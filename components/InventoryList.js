import React from 'react';
import { Text, ActivityIndicator, FlatList, Alert } from 'react-native';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import * as Segment from 'expo-analytics-segment';
import { Cell, Separator } from 'react-native-tableview-simple';
import moment from 'moment';

const dateFormat = 'MMMM Do, YYYY [at] h:mm A';

const PAGE_SIZE = 20;

function rowForCurrentInventory(currentInventory) {
  if (currentInventory) {
    switch (currentInventory.inventoryStatus) {
      case 'waiting_for_spouse':
        return [{ id: 'waiting_for_spouse' }];
      case 'waiting_for_self':
      case 'in_progress':
        return [{ id: 'waiting_for_self' }];
      default:
        return [];
    }
  } else {
    return [];
  }
}

function rowForFirstInventory(inventoryCount) {
  if (inventoryCount === 0) {
    return [{ id: 'first_inventory' }];
  } else {
    return [];
  }
}

class InventoryList extends React.Component {
  componentWillMount() {
    Segment.screen('InventoryList');
  }

  render() {
    let data = this.props.data;
    if (data.networkStatus === 1) {
      return <ActivityIndicator />;
    }

    if (data.error) {
      return <Text>Error: {data.error.message}</Text>;
    }

    const CURRENT_INVENTORY_ROW = rowForCurrentInventory(data.currentInventory);
    const FIRST_INVENTORY_ROW = rowForFirstInventory(
      data.inventories.length + CURRENT_INVENTORY_ROW.length
    );

    // Generate a row for an upgrade prompt if we have not purchased this
    // const UPGRADE_ROW =
    //   this.props.data.purchase && data.purchase.couplePurchase ? [] : [{ id: 'upgrade' }];

    const INITIAL_ROW =
      FIRST_INVENTORY_ROW.length === 0 && CURRENT_INVENTORY_ROW.length === 0
        ? [{ id: 'initial' }]
        : [];

    const LIMITED_HISTORY =
      data.inventories.length > 0 && !(this.props.data.purchase && data.purchase.couplePurchase) > 0
        ? [{ id: 'row_limit' }]
        : [];

    const ROWS = INITIAL_ROW.concat(CURRENT_INVENTORY_ROW)
      .concat(FIRST_INVENTORY_ROW)
      .concat(data.inventories)
      .concat(LIMITED_HISTORY);

    return (
      <FlatList
        style={{ backgroundColor: 'white' }}
        data={ROWS}
        keyExtractor={(item, index) => item.id}
        refreshing={data.networkStatus === 4}
        onRefresh={() => data.refetch()}
        renderItem={({ item, separators }) => {
          switch (item.id) {
            case 'initial':
              return (
                <Cell
                  title={'Everflect now! \u270F\uFE0F'}
                  cellStyle="Basic"
                  onHighlightRow={separators.highlight}
                  onUnHighlightRow={separators.unhighlight}
                  onPress={() => {
                    this.props.startInventoryCb();
                  }}
                />
              );
            case 'waiting_for_spouse':
              return (
                <Cell
                  title={'Pick up where you left off \u270F\uFE0F'}
                  cellStyle="Basic"
                  onHighlightRow={separators.highlight}
                  onUnHighlightRow={separators.unhighlight}
                  onPress={() => {
                    this.props.startInventoryCb();
                  }}
                />
              );
            case 'waiting_for_self':
              return (
                <Cell
                  title={`${this.props.data.spouse.name} is waiting for you \u270F\uFE0F`}
                  cellStyle="Basic"
                  onHighlightRow={separators.highlight}
                  onUnHighlightRow={separators.unhighlight}
                  onPress={() => {
                    this.props.startInventoryCb();
                  }}
                />
              );
            case 'first_inventory':
              return (
                <Cell
                  title={'Start your first inventory! \u270F\uFE0F'}
                  cellStyle="Basic"
                  onHighlightRow={separators.highlight}
                  onUnHighlightRow={separators.unhighlight}
                  onPress={() => {
                    this.props.startInventoryCb();
                  }}
                />
              );
            case 'upgrade':
              return null;
            // return (
            //   <Cell
            //     title={'Upgrade now for more features! \ud83d\ude80'}
            //     cellStyle="Basic"
            //     accessory="DisclosureIndicator"
            //     onHighlightRow={separators.highlight}
            //     onUnHighlightRow={separators.unhighlight}
            //     onPress={() => {
            //       this.props.navigation.navigate('Upgrade');
            //     }}
            //   />
            // );
            case 'row_limit':
              return (
                <Cell
                  title={'More history coming soon \ud83d\ude80'}
                  cellStyle="Basic"
                  onHighlightRow={separators.highlight}
                  onUnHighlightRow={separators.unhighlight}
                  onPress={() => {
                    Alert.alert(
                      'To see the rest of your inventories will require an upgrade (Coming soon)'
                    );
                  }}
                />
              );
            default:
              return (
                <Cell
                  title={moment(item.insertedAt).format(dateFormat)}
                  cellStyle="Basic"
                  onHighlightRow={separators.highlight}
                  onUnHighlightRow={separators.unhighlight}
                  accessory="DisclosureIndicator"
                  onPress={() => {
                    this.props.navigation.navigate('InventoryView', {
                      inventory: item,
                      me: this.props.data.me,
                      spouse: this.props.data.spouse,
                    });
                  }}
                />
              );
          }
        }}
        ItemSeparatorComponent={({ highlighted }) => <Separator isHidden={highlighted} />}
      />
    );
  }
}

InventoryList.propTypes = {
  startInventoryCb: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
};

// Root class for the app interface
const InventoryQuery = gql`
  query InventoryFeed($offset: Int!, $pageSize: Int!, $purchaseKey: String!) {
    currentInventory {
      id
      inventoryStatus
    }
    me {
      id
      name
    }
    spouse {
      id
      name
    }
    inventories(limit: $pageSize, offset: $offset) {
      id
      nextInventory
      inventoryStatus
      spouseGoal
      spouseGoalSupport
      spouseImprovement
      spousePraise1
      spousePraise2
      spousePraise3
      myGoal
      myGoalSupport
      myImprovement
      myPraise1
      myPraise2
      myPraise3
      insertedAt
      updatedAt
    }
    purchase(purchaseKey: $purchaseKey) {
      id
      myPurchase
      couplePurchase
    }
  }
`;

const InventoryListWithData = graphql(InventoryQuery, {
  options: {
    notifyOnNetworkStatusChange: true,
    pollInterval: 1000,
    variables: {
      offset: 0,
      pageSize: PAGE_SIZE,
      purchaseKey: 'all-inventories',
    },
  },
})(InventoryList);

export default InventoryListWithData;
