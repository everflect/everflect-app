import React from 'react';
import { View, Linking } from 'react-native';
import { WebView } from 'react-native-webview';
import PropTypes from 'prop-types';
import Tufte from '../assets/library/tufte';

const injectScript = `
(function () {
  window.onclick = function(e) {
    if (e.target.href !== undefined) {
      e.preventDefault();
      window.postMessage(e.target.href);
      e.stopPropagation()
    }
  }
}());
`;

export default class LessonPage extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.title,
  });

  onMessage({ nativeEvent }) {
    const data = nativeEvent.data;

    if (data !== undefined && data !== null) {
      Linking.openURL(data).catch(() => {});
    }
  }

  render() {
    return (
      <View style={{ height: '100%' }}>
        <WebView
          injectedJavaScript={injectScript}
          onMessage={this.onMessage}
          startInLoadingState
          source={{
            html: `${Tufte}${this.props.navigation.state.params.content}`,
          }}
        />
      </View>
    );
  }
}

LessonPage.propTypes = {
  navigation: PropTypes.object.isRequired,
};
