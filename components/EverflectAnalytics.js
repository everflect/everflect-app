import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import * as Sentry from 'sentry-expo';
import * as Segment from 'expo-analytics-segment';

class EverflectAnalytics extends React.Component {
  render() {
    if (this.props.data.error) {
      return <Text>{JSON.stringify(this.props.data.error)}</Text>;
    }

    if (this.props.data.me !== null && this.props.data.me !== undefined) {
      Sentry.setUser({
        email: this.props.data.me.email,
        user_id: this.props.data.me.id,
        name: this.props.data.me.name,
        device_id: this.props.data.me.deviceId,
      });

      Segment.identify(this.props.data.me.id);
      Segment.identifyWithTraits(this.props.data.me.id, {
        email: this.props.data.me.email,
        user_id: this.props.data.me.id,
        name: this.props.data.me.name,
        device_id: this.props.data.me.deviceId,
      });
    }

    return <View />;
  }
}

EverflectAnalytics.propTypes = {
  data: PropTypes.object.isRequired,
};

const NameQuery = gql`
  query AnalyticsData {
    me {
      id
      deviceId
      name
      email
    }
    couple {
      id
    }
  }
`;
const EverflectAnalyticsWithData = graphql(NameQuery)(EverflectAnalytics);
export default EverflectAnalyticsWithData;
