import React from 'react';
import { Text, View, StyleSheet, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import moment from 'moment';

const dateFormat = 'MMMM Do, YYYY [at] h:mm A';

const styles = StyleSheet.create({
  h1: { fontSize: 24 },
  b: { fontWeight: 'bold' },
  li: {},
});

class InventoryPartialBase extends React.Component {
  render() {
    if (this.props.data.loading) {
      return <ActivityIndicator />;
    }
    const data = this.props.data;
    return (
      <View>
        <Text style={styles.h1}>{data.spouse.name}</Text>
        <Text>
          <Text style={styles.b}>{data.spouse.name}&#39;s goal:</Text> {data.inventory.spouseGoal}
        </Text>
        <Text>
          <Text style={styles.b}>How I will help meet {data.spouse.name}&#39;s goal:</Text>{' '}
          {data.inventory.myGoalSupport}
        </Text>
        <Text>
          <Text style={styles.b}>A suggestion I offered {data.spouse.name}:</Text>{' '}
          {data.inventory.spouseImprovement}
        </Text>
        <Text style={styles.b}>Three things I appreciated about {data.spouse.name}:</Text>
        <Text>&bull; {data.inventory.myPraise1}</Text>
        <Text>&bull; {data.inventory.myPraise2}</Text>
        <Text>&bull; {data.inventory.myPraise3}</Text>

        <Text style={[styles.h1, { marginTop: 15 }]}>{data.me.name}</Text>
        <Text>
          <Text style={styles.b}>My goal:</Text> {data.inventory.myGoal}
        </Text>
        <Text>
          <Text style={styles.b}>How {data.spouse.name} will help me meet my goal:</Text>{' '}
          {data.inventory.spouseGoalSupport}
        </Text>
        <Text>
          <Text style={styles.b}>A suggestion from {data.spouse.name}:</Text>{' '}
          {data.inventory.myImprovement}
        </Text>
        <Text style={styles.b}>Three things {data.spouse.name} appreciated about me:</Text>
        <Text>&bull; {data.inventory.spousePraise1}</Text>
        <Text>&bull; {data.inventory.spousePraise2}</Text>
        <Text>&bull; {data.inventory.spousePraise3}</Text>

        {data.inventory.nextInventory !== null ? (
          <Text style={styles.b}>
            You will Everflect again with {data.spouse.name} on{' '}
            {moment(data.inventory.nextInventory).format(dateFormat)}
          </Text>
        ) : null}
      </View>
    );
  }
}

InventoryPartialBase.propTypes = {
  data: PropTypes.object.isRequired,
  inventoryId: PropTypes.string.isRequired,
};

const InventoryQuery = gql`
  query InventoryPartial($inventoryId: String!) {
    me {
      id
      name
    }
    spouse {
      id
      name
    }
    inventory(inventoryId: $inventoryId) {
      id
      myGoal
      myImprovement
      myGoalSupport
      myPraise1
      myPraise2
      myPraise3
      spouseGoal
      spouseImprovement
      spouseGoalSupport
      spousePraise1
      spousePraise2
      spousePraise3
      nextInventory
    }
  }
`;
const InventoryPartial = graphql(InventoryQuery, {})(InventoryPartialBase);
export default InventoryPartial;
