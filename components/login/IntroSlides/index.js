import WelcomeSlide from './WelcomeSlide';
import ConnectSlide from './ConnectSlide';
import SimpleSlide from './SimpleSlide';

export default WelcomeSlide;
export { WelcomeSlide, ConnectSlide, SimpleSlide };
