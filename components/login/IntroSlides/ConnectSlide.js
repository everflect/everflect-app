import React from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import { iOSUIKit } from 'react-native-typography';
export default class WelcomeSlide extends React.Component {
  render() {
    const { width } = Dimensions.get('window');
    const w = width * 0.4;
    return (
      <View style={{ alignItems: 'center', paddingHorizontal: 20 }}>
        <Text style={[iOSUIKit.title3Emphasized, { color: 'white' }]}>Connect each week</Text>
        <Image
          style={{ height: w, width: w, marginVertical: 50 }}
          source={require('../../../assets/icons/white/png/256/calendar-check-o.png')}
        />
        <Text style={[iOSUIKit.body, { color: 'white', marginTop: 12 }]}>
          Everflect walks you through goals, praise, concerns, feedback, and more! All you need is
          your spouse and a few minutes together!
        </Text>
      </View>
    );
  }
}
