import React from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import { iOSUIKit } from 'react-native-typography';
export default class WelcomeSlide extends React.Component {
  render() {
    const { width } = Dimensions.get('window');
    const w = width * 0.4;
    return (
      <View style={{ alignItems: 'center', paddingHorizontal: 20 }}>
        <Text style={[iOSUIKit.title3Emphasized, { color: 'white' }]}>
          Simple &amp; Easy to use
        </Text>
        <Image
          style={{ height: w, width: w, marginVertical: 50 }}
          source={require('../../../assets/icons/white/png/256/lock.png')}
        />
        <Text style={[iOSUIKit.body, { color: 'white', marginTop: 12 }]}>
          Our prompts and questions are straightforward. Your answers are securely encrypted and
          confidential.
        </Text>
      </View>
    );
  }
}
