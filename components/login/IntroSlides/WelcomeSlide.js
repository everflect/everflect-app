import React from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import { iOSUIKit } from 'react-native-typography';

export default class WelcomeSlide extends React.Component {
  render() {
    const { width } = Dimensions.get('window');
    const w = width * 0.6;

    return (
      <View style={{ alignItems: 'center', paddingHorizontal: 20 }}>
        <Text style={[iOSUIKit.largeTitleEmphasized, { color: 'white' }]}>Welcome to</Text>
        <Image
          style={{ height: w, width: w, marginVertical: 50 }}
          source={require('../../../img/logo/white_alpha.png')}
        />
      </View>
    );
  }
}
