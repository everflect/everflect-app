import React from 'react';
import PropTypes from 'prop-types';
import { View, Dimensions, StatusBar } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import { WelcomeSlide, ConnectSlide, SimpleSlide } from './IntroSlides';
import LoginController from './LoginController';

export default class IntroController extends React.Component {
  constructor(props) {
    super(props);
    const entries = [WelcomeSlide, ConnectSlide, SimpleSlide, LoginController];
    const viewport = {
      width: Dimensions.get('window').width,
    };
    this.state = {
      intro: true,
      animationType: 'none',
      entries,
      viewport,
      activeSlide: 0,
    };

    this._renderItem = this._renderItem.bind(this);
  }

  componentWillReceiveProps(props) {
    if (this.state.intro !== props.visible) {
      this.setState({ intro: true, animationType: 'none', activeSlide: 0 });
    }
  }

  _renderItem({ item: Component, index }) {
    return <Component activeIndex={this.state.activeSlide} index={index} />;
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#90ABDA',
        }}>
        <StatusBar barStyle="light-content" />
        <View
          style={{
            height: '80%',
            width: '100%',
            alignItems: 'center',
          }}>
          <Carousel
            ref={c => {
              this._carousel = c;
            }}
            data={this.state.entries}
            renderItem={this._renderItem}
            sliderWidth={this.state.viewport.width}
            itemWidth={this.state.viewport.width}
            slideStyle={{ width: this.state.viewport.width }}
            onSnapToItem={index => this.setState({ activeSlide: index })}
            inactiveSlideOpacity={1}
            inactiveSlideScale={1}
          />
          <Pagination
            dotsLength={this.state.entries.length}
            activeDotIndex={this.state.activeSlide}
            containerStyle={{
              backgroundColor: 'rgba(0, 0, 0, 0)',
              borderRadius: 20,
              width: '80%',
            }}
            dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              marginHorizontal: 8,
              backgroundColor: 'rgba(255, 255, 255, 0.92)',
            }}
            inactiveDotStyle={{}}
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
            // carouselRef={this._carousel} // Works, but generates a warning??
            // tappableDots
          />
        </View>
      </View>
    );
  }
}
