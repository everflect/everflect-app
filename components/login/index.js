import IntroController from './IntroController';
import LoginController from './LoginController';

export default LoginController;
export { IntroController, LoginController };
