import React from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  Button,
  TextInput,
  ActivityIndicator,
  Image,
  Alert,
  KeyboardAvoidingView,
  Dimensions,
  Platform,
  Linking,
} from 'react-native';
import { iOSUIKit } from 'react-native-typography';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

// Now, you can make a navigator by putting the router on it:
class LoginMain extends React.Component {
  static navigationOptions = {
    title: 'Sign Up/Sign In',
  };

  constructor(props) {
    super(props);

    this.submitLogin = this.submitLogin.bind(this);
    this.state = { email: '', loading: false };
  }

  submitLogin() {
    this.setState({ loading: true });
    this.props
      .mutate({ variables: { email: this.state.email } })
      .then(result => {
        this.setState({ loading: false });
        Alert.alert(
          'Check your email',
          'To finish logging in, click the link in the email we just sent you!',
          [{ text: 'OK' }]
        );
      })
      .catch(error => {
        console.log('Error', error);
        this.setState({ loading: false });
      });
  }

  componentWillReceiveProps(props) {
    if (this.props.activeIndex !== props.activeIndex && props.index === props.activeIndex) {
      this._loginBox.focus();
    } else {
      this._loginBox.blur();
    }
  }

  render() {
    const { width, height } = Dimensions.get('window');
    const w = width * 0.4;
    // Inverse relationship between screen height and keyboard vertical offset
    const h = 65000 / height;

    const buttonColor = Platform.select({
      ios: 'white',
      android: '#738CB7',
    });

    return (
      <KeyboardAvoidingView
        behavior="position"
        keyboardVerticalOffset={h}
        style={{
          paddingTop: '10%',
          padding: '5%',
          backgroundColor: '#90ABDA',
          height: '100%',
        }}>
        <Image
          source={require('../../img/logo/white_alpha.png')}
          style={{
            width: w,
            height: w,
            alignSelf: 'center',
          }}
        />
        <Text
          style={[iOSUIKit.title3, { color: 'white', textAlign: 'center', marginVertical: 30 }]}>
          Enter your email below to get started
        </Text>
        <Text style={[iOSUIKit.subheadEmphasized, { color: 'white' }]}>Email</Text>
        <TextInput
          style={{
            height: 40,
            borderColor: 'gray',
            backgroundColor: 'white',
            color: '#333333',
            borderWidth: 1,
            paddingHorizontal: 10,
          }}
          ref={c => {
            this._loginBox = c;
          }}
          onChangeText={email => this.setState({ email: email.toLowerCase() })}
          onSubmitEditing={this.submitLogin}
          value={this.state.email}
          placeholder="e.g. terry@example.com"
          enablesReturnKeyAutomatically
          returnKeyType="go"
          keyboardType="email-address"
          blurOnSubmit={false}
        />
        {this.state.loading ? (
          <ActivityIndicator style={{ marginTop: 18 }} size="small" color="#ffffff" />
        ) : (
          <Button title="Sign In" color={buttonColor} onPress={this.submitLogin} />
        )}
        <Text style={{ textAlign: 'center', marginVertical: 30 }}>
          <Text style={[iOSUIKit.caption, { color: 'white' }]}>By signing up you agree to our</Text>{' '}
          <Text
            onPress={() => {
              Linking.openURL('https://everflect.com/privacy-policy/');
            }}
            style={[
              iOSUIKit.caption,
              {
                color: 'white',
                textAlign: 'center',
                textDecorationLine: 'underline',
                marginVertical: 30,
              },
            ]}>
            terms of service.
          </Text>
        </Text>
      </KeyboardAvoidingView>
    );
  }
}

LoginMain.propTypes = {
  mutate: PropTypes.func.isRequired,
  activeIndex: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
};

const LoginMutation = gql`
  mutation login($email: String!) {
    requestLoginEmail(email: $email) {
      id
      status
    }
  }
`;
const LoginMainWithMutation = graphql(LoginMutation)(LoginMain);
export default LoginMainWithMutation;
