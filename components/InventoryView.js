import React from 'react';
import { View, ScrollView, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';

import InventoryPartial from './InventoryPartial';

const dateFormat = 'L LT';

export default class InventoryView extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: moment(navigation.state.params.inventory.insertedAt).format(dateFormat),
  });

  constructor(props) {
    super(props);
    this.state = { scrollable: false };
  }

  onLayout(event) {
    const newHeight = event.nativeEvent.layout.height;
    const { height } = Dimensions.get('window');
    if (newHeight > height) {
      this.setState({ scrollable: true });
    } else {
      this.setState({ scrollable: false });
    }
  }

  render() {
    return (
      <ScrollView
        scrollEnabled={this.state.scrollable}
        onContentSizeChange={(_width, height) => {
          const windowHeight = Dimensions.get('window').height * 0.8;
          if (height > windowHeight) {
            this.setState({ scrollable: true });
          } else {
            this.setState({ scrollable: false });
          }
        }}>
        <View style={{ height: '100%', marginHorizontal: 10 }}>
          <InventoryPartial inventoryId={this.props.navigation.state.params.inventory.id} />
        </View>
      </ScrollView>
    );
  }
}

InventoryView.propTypes = {
  navigation: PropTypes.object.isRequired,
};
