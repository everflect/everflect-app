import React from 'react';
import { View, Linking } from 'react-native';
import { WebView } from 'react-native-webview';
import Tufte from '../../assets/library/tufte';
import qanda_text from '../../assets/qanda';

export default class QandAPage extends React.Component {
  onMessage({ nativeEvent }) {
    const data = nativeEvent.data;

    if (data !== undefined && data !== null) {
      Linking.openURL(data).catch(() => {});
    }
  }
  render() {
    return (
      <View style={{ height: '100%' }}>
        <WebView
          onMessage={this.onMessage}
          source={{
            html: `${Tufte}${qanda_text}`,
          }}
        />
      </View>
    );
  }
}
