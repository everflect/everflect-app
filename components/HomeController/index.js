import React from 'react';
import { StyleSheet, Image, StatusBar, Platform, View, NativeModules } from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';
import PropTypes from 'prop-types';

import InventoryList from '../InventoryList';
import InventoryView from '../InventoryView';
import UpgradeView from '../UpgradeView';

import SettingsPage from '../SettingsPage';
import LicensesPage from './LicensesPage';
import QandAPage from './QandAPage';

import LibraryPage from '../LibraryPage';
import LessonPage from '../LessonPage';

const styles = StyleSheet.create({
  icon: {
    width: 26,
    height: 26,
  },
});

class InventoryStack extends React.Component {
  static navigationOptions = {
    title: 'Everflections',
  };

  render() {
    return (
      <InventoryList
        navigation={this.props.navigation}
        startInventoryCb={this.props.screenProps.startInventoryCb}
      />
    );
  }
}

InventoryStack.propTypes = {
  screenProps: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
};

class SettingsStack extends React.Component {
  static navigationOptions = {
    title: 'Settings',
  };

  render() {
    return <SettingsPage navigate={this.props.navigation.navigate} />;
  }
}

SettingsStack.propTypes = {
  // screenProps: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
};

const InventoryNavigator = StackNavigator({
  InventoryList: {
    screen: InventoryStack,
  },
  InventoryView: {
    screen: InventoryView,
  },
  Upgrade: {
    screen: UpgradeView,
  },
});

const SettingsNavigator = StackNavigator({
  Settings: {
    screen: SettingsStack,
  },
  Licenses: {
    screen: LicensesPage,
    navigationOptions: () => ({
      title: 'Licenses',
    }),
  },
  QandA: {
    screen: QandAPage,
    navigationOptions: () => ({
      title: 'Q & A',
    }),
  },
});

const LibraryNavigator = StackNavigator({
  Library: {
    screen: LibraryPage,
  },
  Lesson: {
    screen: LessonPage,
  },
  Upgrade: {
    screen: UpgradeView,
  },
});

class InventoryTab extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Everflections',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../../assets/icons/white/png/256/list.png')}
        style={[styles.icon, { tintColor }]}
      />
    ),
  };

  render() {
    return (
      <InventoryNavigator
        screenProps={{
          startInventoryCb: this.props.screenProps.startInventoryCb,
        }}
      />
    );
  }
}

InventoryTab.propTypes = {
  screenProps: PropTypes.object.isRequired,
};

class SettingsTab extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Settings',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../../assets/icons/white/png/256/cog.png')}
        style={[styles.icon, { tintColor }]}
      />
    ),
  };

  render() {
    return <SettingsNavigator />;
  }
}

class LibraryTab extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Library',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../../assets/icons/white/png/256/book.png')}
        style={[styles.icon, { tintColor }]}
      />
    ),
  };

  render() {
    return <LibraryNavigator />;
  }
}

const homeNavigatorTabs = {
  Inventories: {
    screen: InventoryTab,
  },
};

if (NativeModules.RNIapIos !== undefined || NativeModules.RNIapModule !== undefined) {
  homeNavigatorTabs.Library = {
    screen: LibraryTab,
  };
}

homeNavigatorTabs.Settings = {
  screen: SettingsTab,
};

const HomeNavigator = TabNavigator(homeNavigatorTabs, {
  animationEnabled: false,
  tabBarOptions: Platform.select({
    ios: {},
    android: {
      indicatorStyle: {
        backgroundColor: '#FFFFFF',
      },
      style: {
        paddingTop: StatusBar.currentHeight,
        backgroundColor: '#90ABDA',
      },
    },
  }),
});

export default class HomeController extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          barStyle={Platform.select({
            ios: 'dark-content',
            android: 'light-content',
          })}
        />
        <HomeNavigator screenProps={{ startInventoryCb: this.props.startInventoryCb }} />
      </View>
    );
  }
}

HomeController.propTypes = {
  startInventoryCb: PropTypes.func.isRequired,
};
