import React from 'react';
import { View } from 'react-native';
import { WebView } from 'react-native-webview';
import Tufte from '../../assets/library/tufte';
import license_text from '../../assets/licenses';

export default class LicensesPage extends React.Component {
  render() {
    return (
      <View style={{ height: '100%' }}>
        <WebView
          source={{
            html: `${Tufte}${license_text}`,
          }}
        />
      </View>
    );
  }
}
