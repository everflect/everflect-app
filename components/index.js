import InventoryController from './InventoryController';
import Loader from './Loader';
import RootControllerContainer from './RootController';
import HomePage from './HomePage';
import SettingsPage from './SettingsPage';
import EverflectAnalytics from './EverflectAnalytics';

export default {
  InventoryController,
  Loader,
  RootControllerContainer,
  HomePage,
  SettingsPage,
  EverflectAnalytics,
};

export { InventoryController };
export { Loader };
export { RootControllerContainer };
export { HomePage };
export { SettingsPage };
export { EverflectAnalytics };
