// import { Permissions, Segment } from 'expo';
// import { AsyncStorage } from 'react-native';

/*
{
    title: 'Notifications are now available!',
    icon: '\uf08a',
    message:
      'Everflect can now remind you during the week to review goal progress with your spouse and when it is time for your next Everflect session.',
    button: {
      title: 'Enable notifications',
      onPress: () => {
        Permissions.askAsync(Permissions.NOTIFICATIONS).then(({ status }) => {
          if (status !== 'granted') {
            Segment.trackWithProperties('DeniedPermissions', {
              permission: 'notifications',
              where: 'End of inventory',
            });
          }
          return AsyncStorage.setItem('@EF:PermissionsRequested.Notifications', 'true');
        });
      },
    },
  },
  {
    title: 'Course content',
    icon: '\uf02d',
    message:
      'Our affordable courses feature content curated to help you make the most of your Everflect sessions',
  },
*/

export default [];
