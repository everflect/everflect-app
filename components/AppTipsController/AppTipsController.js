import React from 'react';
import { Text, View, Button, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import Carousel from 'react-native-snap-carousel';

import AppTips from './AppTips';

export default class AppTipsController extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      entries: AppTips,
      viewport: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
      },
    };

    this._renderItem = this._renderItem.bind(this);
  }
  _renderItem({ item, index }) {
    return (
      <View
        style={{
          height: '80%',
          margin: '10%',
          padding: '10%',
          backgroundColor: '#3cace0',
          borderRadius: 20,
        }}>
        <Text
          style={{
            fontFamily: 'fontawesome',
            fontSize: 48,
            textAlign: 'center',
            color: 'white',
          }}>
          {item.icon}
        </Text>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 24,
            fontWeight: 'bold',
            color: 'white',
          }}>
          {item.title}
        </Text>
        <Text style={{ color: 'white' }}>{item.message}</Text>
        {item.button ? (
          <Button color="white" title={item.button.title} onPress={item.button.onPress} />
        ) : null}
        {index + 1 === AppTips.length ? (
          <Button color="white" title="Close" onPress={this.props.clearAppTipsCb} />
        ) : null}
      </View>
    );
  }

  render() {
    return (
      <View
        onLayout={() => {
          this.setState({
            viewport: {
              width: Dimensions.get('window').width,
              height: Dimensions.get('window').height,
            },
          });
        }}
        style={{ width: '100%', height: '100%' }}>
        <Carousel
          ref={c => {
            this._carousel = c;
          }}
          data={this.state.entries}
          renderItem={this._renderItem}
          sliderWidth={this.state.viewport.width}
          itemWidth={this.state.viewport.width}
          slideStyle={{ width: this.state.viewport.width }}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
        />
      </View>
    );
  }
}

AppTipsController.propTypes = {
  clearAppTipsCb: PropTypes.func.isRequired,
};
