import AppTipsController from './AppTipsController';
import AppTips from './AppTips';

export default {
  AppTipsController,
  AppTips,
};

export { AppTipsController };
export { AppTips };
