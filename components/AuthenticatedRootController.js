import React from 'react';
import PropTypes from 'prop-types';
import { View, Modal, AsyncStorage, StatusBar } from 'react-native';
import { graphql, compose } from 'react-apollo';
import Constants from 'expo-constants';
import gql from 'graphql-tag';

import HomeController from './HomeController';
import { AppTips } from './AppTipsController';
import { OnboardingControllerWithData } from './onboarding';
import InventoryModal from './InventoryModal';
import EverflectAnalytics from './EverflectAnalytics';
import PushNotificationManager from '../managers/PushNotificationManager';

// Root class for the app interface
class AuthenticatedRootController extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      appTips: false,
      onboarding: false,
      inventoryRunning: false,
    };

    this.clearAppTipsCb = this.clearAppTipsCb.bind(this);
    this.onboardingCompleteCb = this.onboardingCompleteCb.bind(this);
    this.startInventoryCb = this.startInventoryCb.bind(this);
    this.endInventoryCb = this.endInventoryCb.bind(this);
    this.suspendInventoryCb = this.suspendInventoryCb.bind(this);
    this._registerPush = this._registerPush.bind(this);
    this.registerForPushNotifications = this.registerForPushNotifications.bind(this);
  }

  componentWillReceiveProps(props) {
    if (props.data.loading) {
      return;
    }
    if (!this.state.onboarding && props.data && props.data.spouse === null) {
      this.setState({
        onboarding: true,
      });
    }

    AsyncStorage.getItem('@EF:ViewedAppTips').then(val => {
      if (val === null || parseInt(val, 10) < AppTips.length) {
        this.setState({
          appTips: true,
        });
      } else {
        AsyncStorage.setItem('@EF:ViewedAppTips', AppTips.length.toString());
      }
    });
  }

  registerForPushNotifications() {
    if (this._registerForPushNotificationsFn) {
      this._registerForPushNotificationsFn();
    } else {
      setTimeout(() => {
        this.registerForPushNotifications();
      }, 500);
    }
  }

  clearAppTipsCb() {
    AsyncStorage.setItem('@EF:ViewedAppTips', AppTips.length.toString()).then(() => {
      this.setState({
        appTips: false,
      });
    });
  }

  onboardingCompleteCb() {
    this.setState({
      onboarding: false,
    });
  }

  startInventoryCb() {
    this.props.mutate().then(_results => {
      setTimeout(() => {
        this.setState({
          inventoryRunning: true,
        });
      });
    });
  }

  endInventoryCb() {
    this.setState({
      inventoryRunning: false,
    });
    if (this._registerForPushNotificationsFn) {
      this._registerForPushNotificationsFn();
    }
  }

  suspendInventoryCb() {
    this.setState({
      inventoryRunning: false,
    });
  }

  _registerPush(registerForPushNotificationsFn) {
    this._registerForPushNotificationsFn = registerForPushNotificationsFn;
  }

  render() {
    if (this.props.data.loading) {
      return <View />;
    } else {
      return (
        <View
          style={{
            height: '100%',
          }}>
          <StatusBar barStyle="dark-content" />
          {/* <Modal animationType="slide" transparent={false} visible={this.state.appTips}>
            <View
              style={{
                height: '100%',
                marginTop: Constants.statusBarHeight * 2,
              }}>
              <AppTipsController clearAppTipsCb={this.clearAppTipsCb} />
            </View>
          </Modal> */}

          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.onboarding}
            onRequestClose={() => {}}>
            <View
              style={{
                height: '100%',
                marginTop: Constants.statusBarHeight * 2,
              }}>
              <OnboardingControllerWithData onboardingComplete={this.onboardingCompleteCb} />
            </View>
          </Modal>

          <InventoryModal
            visible={this.state.inventoryRunning}
            endInventoryCb={this.endInventoryCb}
            suspendInventoryCb={this.suspendInventoryCb}
          />

          {!this.state.onboarding ? (
            <HomeController startInventoryCb={this.startInventoryCb} />
          ) : null}
          <PushNotificationManager registerFn={this._registerPush} />
          <EverflectAnalytics />
        </View>
      );
    }
  }
}

AuthenticatedRootController.propTypes = {
  data: PropTypes.object.isRequired,
  mutate: PropTypes.func.isRequired,
};

const RootQuery = gql`
  query RootController {
    spouse {
      id
    }
  }
`;

const StartInventoryMutation = gql`
  mutation StartInventory {
    startInventory {
      id
    }
  }
`;

const AuthenticatedRootControllerWithMutation = compose(
  graphql(RootQuery, {
    options: {
      pollInterval: 1000,
    },
  }),
  graphql(StartInventoryMutation)
)(AuthenticatedRootController);

export default AuthenticatedRootControllerWithMutation;
