import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import * as Segment from 'expo-analytics-segment';

import {
  StepWaiting,
  Inventory_Step1,
  Inventory_Step2,
  Inventory_Step3,
  Inventory_Step4,
  Inventory_Step5,
  Inventory_Step6,
  Inventory_Step7,
  Inventory_Step8,
  Inventory_Step9,
  Inventory_Step10,
  Inventory_Step11,
  Inventory_Step12,
  Inventory_Step13,
} from './inventory_steps';
import styles from './inventory_steps/styles';
import EFButton from './inventory_steps/ef_button';

export default class InventoryBody extends React.Component {
  render() {
    // Loading...
    if (this.props.data.loading || this.props.data.currentInventory === undefined) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size="large" />
        </View>
      );
    } else if (
      // Completed
      this.props.data.currentInventory === null ||
      this.props.data.currentInventory.inventoryStatus === 'completed'
    ) {
      return (
        <View style={styles.flexWrapper}>
          <View style={styles.stepWrapper}>
            <Text style={styles.text}>Your inventory is now complete! Come back next week!</Text>
          </View>

          <EFButton onPress={this.props.endInventoryCb} label="Ok, Ready!" />
        </View>
      );
    } else if (this.props.data.currentInventory.inventoryStatus === 'waiting_for_spouse') {
      return <Text>Waiting for {this.props.data.spouse.name}...</Text>;
    } else if (
      // Ahead of spouse
      this.props.data.currentInventory.myStep - this.props.data.currentInventory.spouseStep >
      0
    ) {
      return <StepWaiting />;
    } else {
      // On a step
      let STEP_ELEMENT = null;
      Segment.screen(`Inventory_Step${this.props.data.currentInventory.myStep}`);
      switch (this.props.data.currentInventory.myStep) {
        case 1:
          STEP_ELEMENT = Inventory_Step1;
          break;
        case 2:
          STEP_ELEMENT = Inventory_Step2;
          break;
        case 3:
          STEP_ELEMENT = Inventory_Step3;
          break;
        case 4:
          STEP_ELEMENT = Inventory_Step4;
          break;
        case 5:
          STEP_ELEMENT = Inventory_Step5;
          break;
        case 6:
          STEP_ELEMENT = Inventory_Step6;
          break;
        case 7:
          STEP_ELEMENT = Inventory_Step7;
          break;
        case 8:
          STEP_ELEMENT = Inventory_Step8;
          break;
        case 9:
          STEP_ELEMENT = Inventory_Step9;
          break;
        case 10:
          STEP_ELEMENT = Inventory_Step10;
          break;
        case 11:
          STEP_ELEMENT = Inventory_Step11;
          break;
        case 12:
          STEP_ELEMENT = Inventory_Step12;
          break;
        case 13:
          STEP_ELEMENT = Inventory_Step13;
          break;
        default:
          return <Text>Unknown Step {this.props.data.currentInventory.myStep}?</Text>;
      }
      return <STEP_ELEMENT waiting={this.props.waiting} spouseName={this.props.spouseName} />;
    }
  }
}

InventoryBody.propTypes = {
  data: PropTypes.object.isRequired,
  endInventoryCb: PropTypes.func.isRequired,
  waiting: PropTypes.bool.isRequired,
  spouseName: PropTypes.string,
};
