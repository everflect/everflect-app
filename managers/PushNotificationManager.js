import React from 'react';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { View, AsyncStorage, Platform } from 'react-native';

class PushNotificationManagerBase extends React.Component {
  constructor(props) {
    super(props);

    this.props.registerFn(this.registerForPushNotifications.bind(this));
  }

  async refreshPushNotificationToken() {
    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    if (status !== 'granted') {
      return;
    }
    return await this._sendPushToken();
  }

  async registerForPushNotifications() {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
      return;
    }
    return await this._sendPushToken();
  }

  async _sendPushToken() {
    // Get the token that uniquely identifies this device
    const options = Platform.select({
      ios: {},
      android: {
        gcmSenderId: '830875916194',
      },
    });
    try {
      const { type, data: pushToken } = await Notifications.getDevicePushTokenAsync(options);
      const platform = type === 'apns' ? 'ios' : 'android';

      await this.props.mutate({
        variables: {
          pushToken,
          platform,
        },
      });
      return await AsyncStorage.setItem('@EF:PermissionsRequested.Notifications', 'true');
    } catch (e) {
      // Ignore mutation errors - failures to submit the push token are not fatal
    }
  }

  componentDidMount() {
    this.refreshPushNotificationToken();
  }
  render() {
    return (
      <View
        style={{
          display: 'none',
        }}
      />
    );
  }
}
const pushNotificationMutation = gql`
  mutation RegisterPushToken($pushToken: String!, $platform: String!) {
    registerPushToken(pushToken: $pushToken, platform: $platform) {
      id
    }
  }
`;
const PushNotificationManager = graphql(pushNotificationMutation)(PushNotificationManagerBase);

export default PushNotificationManager;
