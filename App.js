/* Holiness to the Lord */

import React from 'react';
import { ErrorRecovery, AppLoading, Notifications } from 'expo';
import * as Segment from 'expo-analytics-segment';
import * as Font from 'expo-font';
import Constants from 'expo-constants';
import { Asset } from 'expo-asset';
import { Alert, Linking } from 'react-native';
import PropTypes from 'prop-types';

// Setup error logging
import * as Sentry from 'sentry-expo';
// import { SentrySeverity, SentryLog } from 'react-native-sentry';

import EverflectProvider from './EverflectProvider';
import { RootControllerContainer } from './components';

Sentry.init({
  dsn: 'https://d3fb9af7d091417996205fd69bd70abc@sentry.io/207670',
  enableInExpoDevelopment: false,
  debug: true
});

ErrorRecovery.setRecoveryProps({
  crashed: true
});

// Setup Segment Analytics
try {
  Segment.initialize({
    androidWriteKey: 'avWY7xCH3qjCQcWPbUMfehjoIOAtPeeN',
    iosWriteKey: 'DU9pr7lsbL8HgLWEf28e42vlQtV774ck'
  });
} catch (e) {
  // Ignore errors here
}

// Clear badge number
Notifications.setBadgeNumberAsync(0);

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}
export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = { isReady: false };
    this._applicationLoading = this._applicationLoading.bind(this);
    this._handleOpenURL = this._handleOpenURL.bind(this);
  }

  componentDidMount() {
    // Present option to report errors to users
    if (this.props.exp.errorRecovery !== undefined) {
      Linking.canOpenURL('mailto://hello@everflect.com').then(canOpen => {
        if (canOpen) {
          Alert.alert(
            'Oops!',
            'Everflect crashed recently. Would you mind sending us a note about it?',
            [
              { text: 'No, thanks', onPress: () => {} },
              {
                text: 'Sure!',
                onPress: () => {
                  Linking.openURL(
                    'mailto://hello@everflect.com?subject=Everflect%20Crashed&body=Tell%20us%20what%20happened.%20Thanks%20for%20your%20feedback!'
                  );
                }
              }
            ],
            { cancelable: false }
          );
        }
      });
    }

    // Handle deep linking authentication
    Linking.getInitialURL().then(url => {
      if (
        url !== Constants.linkingUri.slice(0, Constants.linkingUri.length - 2)
      ) {
        this.navigate(url);
      }
    });
    Linking.addEventListener('url', this._handleOpenURL);
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this._handleOpenURL);
  }

  _handleOpenURL(event) {
    this.navigate(event.url);
  }

  navigate(url) {
    const authMatch = url.match(/\/login\/token\/([0-9A-Za-z]+)/);
    if (authMatch !== null && authMatch[1].match(/[a-zA-Z0-9]{30}/) !== null) {
      const authToken = authMatch[1];
      this._consumeLoginToken(authToken);
    }
  }

  _consumeLoginToken(authToken) {
    if (this._apolloClient === undefined) {
      setTimeout(() => {
        this._consumeLoginToken(authToken);
      }, 100);
    } else {
      this._apolloClient._handleLoginTokenExchange(authToken);
    }
  }

  async _applicationLoading() {
    const fontAssets = Font.loadAsync({
      fontawesome: require('./assets/fonts/fontawesome-webfont.ttf'),
      courier: require('./assets/fonts/courier.ttf'),
      courierBold: require('./assets/fonts/courier-bold.ttf')
    });

    const imageAssets = cacheImages([
      require('./img/logo/white_alpha.png'),
      require('./assets/icons/white/png/256/calendar-check-o.png'),
      require('./assets/icons/white/png/256/cog.png'),
      require('./assets/icons/white/png/256/list.png'),
      require('./assets/icons/white/png/256/book.png'),
      require('./assets/icons/white/png/256/lock.png')
    ]);

    await Promise.all([fontAssets, ...imageAssets]);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._applicationLoading}
          onFinish={() => this.setState({ isReady: true })}
        />
      );
    }

    return (
      <EverflectProvider
        ref={c => (this._apolloClient = c)}
        isDevelopment={
          this.props.exp.manifest.packagerOpts &&
          this.props.exp.manifest.packagerOpts.dev
        }
      >
        <RootControllerContainer />
      </EverflectProvider>
    );
  }
}

App.propTypes = {
  exp: PropTypes.object.isRequired
};
