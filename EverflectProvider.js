import React from 'react';
import { AsyncStorage, Alert } from 'react-native';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { onError } from 'apollo-link-error';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';

export default class EverflectProvider extends React.Component {
  constructor(props) {
    super(props);

    // Setup Apollo Client
    this._sessionTokenCache = null;
    AsyncStorage.getItem('@EF:SessionToken').then(sessionToken => {
      if (sessionToken !== null) {
        this._handleSessionTokenChange(sessionToken);
      }
    });

    let networkInterfaceOptions = {};
    if (props.isDevelopment && false) {
      console.log('Connecting to local GraphQL Endpoint');
      networkInterfaceOptions = {
        uri: `http://192.168.1.16:4000/api/graphql`
      };
    } else {
      console.log('Connecting to remote GraphQL Endpoint');

      networkInterfaceOptions = {
        uri: 'https://api.everflect.com/api/graphql'
      };
    }

    const cache = new InMemoryCache();

    const httpLink = createHttpLink(networkInterfaceOptions);

    const authLink = setContext(() => {
      if (typeof this._sessionTokenCache === 'string') {
        return {
          headers: {
            'x-everflect-auth-token': this._sessionTokenCache
          }
        };
      } else {
        return {};
      }
    });

    const errorLink = onError(({ response, graphQLErrors, networkError }) => {
      console.log('GQL ERROR');
      if (graphQLErrors) {
        const auth_required = graphQLErrors.reduce(
          (auth_required, { message }) =>
            auth_required || message === 'Authentication required',
          false
        );

        if (auth_required) {
          response.errors = null;
          console.log(
            'Auth required - reset client store to clear authentication token'
          );
          this._handleSessionTokenChange(null);
        }

        graphQLErrors
          .filter(error => error.message !== 'Authentication required')
          .map(({ message, locations, path }) =>
            console.log(
              `[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(
                locations
              )}, Path: ${path}`
            )
          );
      }

      if (networkError) {
        console.log(`[Network error]: ${networkError}`);
      }
    });

    const link = ApolloLink.from([errorLink, authLink, httpLink]);

    this._client = new ApolloClient({
      link,
      cache,
      resolvers: {
        Mutation: {
          updateSessionToken: async (_, { sessionToken }, { cache }) => {
            if (sessionToken === this._sessionTokenCache) {
              return {
                loggedIn: typeof sessionToken === 'string'
              };
            }
            if (sessionToken === null) {
              await AsyncStorage.removeItem('@EF:SessionToken');
              setTimeout(() => {
                this._client.resetStore();
              }, 0);
            } else {
              await AsyncStorage.setItem('@EF:SessionToken', sessionToken);
            }
            this._sessionTokenCache = sessionToken;

            const data = {
              deviceAuthentication: {
                __typename: 'DeviceAuthentication',
                loggedIn: typeof sessionToken === 'string'
              }
            };

            cache.writeData({
              data
            });
            return {
              loggedIn: typeof sessionToken === 'string'
            };
          }
        },
        Query: {
          deviceAuthentication: async () => {
            const sessionToken = this._sessionTokenCache;
            return {
              __typename: 'DeviceAuthentication',
              loggedIn: typeof sessionToken === 'string'
            };
          }
        }
      }
    });

    cache.writeData({
      data: {
        deviceAuthentication: {
          __typename: 'DeviceAuthentication',
          loggedIn: typeof this._sessionTokenCache === 'string'
        }
      }
    });
  }

  async _handleLoginTokenExchange(loginToken) {
    try {
      const results = await this._client.mutate({
        mutation: gql`
          mutation HandleLoginTokenExchange($loginToken: String!) {
            exchangeTokenForSession(token: $loginToken) {
              id
              session
            }
          }
        `,
        variables: {
          loginToken
        },
        refetchQueries: [
          {
            query: gql`
              query RootController {
                deviceAuthentication @client {
                  loggedIn
                }
              }
            `
          }
        ]
      });

      const sessionToken = results.data.exchangeTokenForSession.session;

      await this._handleSessionTokenChange(sessionToken);
    } catch (e) {
      console.log(e);
      Alert.alert(
        'Login token expired',
        'Oops! Try logging in again. The link in your email expires after 5 mintues.'
      );
    }
  }

  async _handleSessionTokenChange(sessionToken) {
    await this._client.mutate({
      mutation: gql`
        mutation HandleSessionTokenChange($sessionToken: String!) {
          updateSessionToken(sessionToken: $sessionToken) @client
        }
      `,
      variables: {
        sessionToken
      },
      refetchQueries: [
        {
          query: gql`
            query RootController {
              deviceAuthentication @client {
                loggedIn
              }
            }
          `
        }
      ]
    });
  }

  render() {
    return (
      <ApolloProvider client={this._client}>
        {this.props.children}
      </ApolloProvider>
    );
  }
}

EverflectProvider.propTypes = {
  isDevelopment: PropTypes.bool.isRequired,
  children: PropTypes.object.isRequired
};
