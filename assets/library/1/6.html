<article>
    <p>
        In the previous lesson, we discussed becoming more fluent in your spouse’s love language. It is hard to become fluent if
        you are not taking the time and effort to get to know them and practice.
        <strong>In this lesson, we will discuss how to better prioritize and balance your life in a meaningful way. </strong>
    </p>

    <section>
        <h1>Prioritization</h1>
        <p class="caption">Balance Your Life</p>
        <figure class="fullwidth">
            <img src="https://gallery.mailchimp.com/f0f955bc59f272fc94ff6d1a3/images/cbf11b66-0ce4-4f39-aac6-6af1f61e36d5.jpg" alt="A balanced stack of stones on a beach"
            />
        </figure>

        <p>The secret to living a full life is two fold. First, put the most important things first. Second, fill the rest with
            variety.
        </p>

        <h2>Put the Most Important Things First</h2>
        <p>There is a story that has been told many times. Imagine an empty jar - representing your life - sitting on a table.
            The jar is then filled with large rocks. It appears to be full. But then, a bag of pebbles is poured inside and
            they fill in around the rocks. Now, it must be full. But then, a bag of sand is poured in filling between the
            rocks and sand. Certainly it is full now. But then, a bottle of water is poured, rushing through until it overflows. </p>

        <p>Is the moral of this story that you should fit as much into your life as possible? No. The moral of the story is
            that everything would only fit if you put the big rocks in first. If you had started with the sand, it would
            layer up until there was no room for the large rocks. When you prioritize your life, you can fit an abundance
            in. If you fill your life with pebbles and sand first, you will never have time for the big rocks that matter. </p>

        <p>Everflect was created was to help you put your big rocks first. If you prioritize your life to make time to Everflect
            with your spouse, you will find that life will be much more fulfilling. Once you have set aside the time to counsel
            with your spouse and put that "big rock" in place, you can then take the time to fill your life with a beautiful
            variety of pebbles, sand, and water.</p>

        <p>Your spouse, their love languages, their needs, and who they are represent a "big rock". Start now to prioritize
            these in a non-negotiable way. The story of the jar is sometimes told in such a way that we are led to believe
            that we can fit everything in if we just prioritize properly. While this is partly true, it is important to remember
            that we may need to let some things flow out of the jar. Some things are not meant to fit in the jar, but if
            we put the most important things in first, like our marriage and family, then our lives will be full of what
            actually matters.</p>

        <h2>Fill the Rest with Variety</h2>
        <p>After you have put your big rocks first, sometimes the pebbles and rocks also get imbalanced. You may be pouring
            the pebbles down one side of the jar, while leaving a huge gap in the other. In order to find a better balance
            in your progression as a couple, choose goals each week from a variety of areas in life. There are several ways
            to categorize a balanced life, but for our purposes here we will use these six categories:</p>

        <ul>
            <li>
                <strong>Physical:</strong> food, exercise, sleep, doctor visits, etc.</li>
            <li>
                <strong>Intellectual:</strong> school, budgets, reading, etc.</li>
            <li>
                <strong>Service:</strong> reaching out to others formally or informally</li>
            <li>
                <strong>Social:</strong> spending time together with other family or friends</li>
            <li>
                <strong>Creative:</strong> art, music, dance, fun home improvement projects, etc.</li>
            <li>
                <strong>Spiritual:</strong> religion, nature, peace, emotional well-being, etc.</li>
        </ul>

        <p>Obviously goals will often overlap between categories, but if your Everflection has started to feel more like a weekly
            budget hash-out or a meal-planning session, then it is likely time for you to re-balance your conversations.
            If you have become too focused on one area of life, you are unlikely to get forward traction with
            <em>any</em> of your weekly goals.</p>

        <p>This, however, does not mean going overboard with several goals in each area.</p>

        <blockquote>
            <p>"To go beyond is as wrong as to fall short"</p>
            <footer>- Confucius</footer>
        </blockquote>

        <p>This is why Everflect only has a limited number of spaces to record goals. You will make the fastest progression
            as a couple if you first prioritize each other and then focus on a few - and a diverse few - goals at time. </p>

        <p>Do not neglect one of these categories of life until it has totally pulled you off balance. And especially take care
            not to neglect the big stone of your marriage and the time you set aside to Everflect. After all - even green
            grass needs watering.
        </p>

        <blockquote>
            <p>"If you defer investing your time and energy until you see that you need to, chances are it will already be too
                late."
            </p>
            <footer>- Clayton M. Christensen
                <label for="sn-investing-timing" class="margin-toggle sidenote-number">
                </label>
                <input type="checkbox" id="sn-investing-timing" class="margin-toggle" />
                <span class="sidenote">
                    <em>Christensen, Clayton M., James Allworth, and Karen Dillon. How Will You Measure Your Life? New York,
                        NY: Harper Business, 2012.</em>
                </span>
            </footer>
        </blockquote>
    </section>

    <figure class="fullwidth">
        <img src="https://gallery.mailchimp.com/f0f955bc59f272fc94ff6d1a3/images/1aec40fe-fe76-4221-80b2-860f4cbf26ba.jpg" alt="A blank sketchbook with a pencil on a wooden floor"
        />
    </figure>

    <section>
        <h1>Activity</h1>

        <p>Get out a piece of paper and make three columns. Title the first column "Rocks", the second "Pebbles", and the third
            "Sand".
        </p>
        <p>
            <strong>In the "Rocks" section</strong>, discuss and write down what your highest priorities are (the non-negotiable).
            For example, your relationship with God, Spouse, and Children.</p>

        <p>
            <strong>In the "Pebbles" section</strong>, discuss and write down what your high priorities are that are still negotiable.
            For example, your work, service, or your time with close friends.</p>

        <p>
            <strong>In the "Sand" column</strong>, discuss and write down other things that are important to you, but do not qualify
            for the other two columns. For example, spending time on a hobby, going to the gym, or watching a favorite episode. </p>

        <p>Reflect on your lists and discuss ways in which you can re-prioritize according to the columns. If a "sand" item
            is interrupting a "rock" item, drop it out. </p>



        <h2>Pro Tip</h2>
        <p>Plan a date night once a week for the next 6 weeks in all 6 categories. Here are some balanced (and free!) date night
            ideas:
        </p>
        <ul>
            <li>Physical - go for a hike</li>
            <li>Intellectual  - read at the library</li>
            <li>Service - volunteer to help children with special needs</li>
            <li>Social - host a game night with other couples</li>
            <li>Creative - sketch a portrait of each other</li>
            <li>Spiritual - go star gazing</li>
        </ul>
        <p>Check out
            <a href="https://www.pinterest.com/everflect/">our Pinterest page</a> or
            <a href="https://www.facebook.com/groups/546711192380150/">our Facebook group</a> for more ideas.</p>
        <hr/>
    </section>
    <section>

        <figure class="fullwidth">
            <img src="https://gallery.mailchimp.com/f0f955bc59f272fc94ff6d1a3/images/99d984f3-9abf-4eb6-9c21-d13ef549d70e.jpg" alt="Piles of closed and open books"
            />
        </figure>
        <h1>Suggested Study</h1>
        <ul>
            <li>
                <em>How will you Measure your Life?</em> by Clayton M. Christensen</li>
        </ul>
    </section>
</article>