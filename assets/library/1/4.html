<article>
    <p>In the last lesson, we discussed content communication. As you make strides toward communicating better, you must also
        listen better.
        <strong>In this lesson, we will discuss better ways to hear and validate what your spouse does, what they say, and who they
            are.
        </strong>
    </p>

    <h1>Validation</h1>
    <p class="caption">Recognize and Affirm</p>
    <figure class="fullwidth">
        <img src="https://gallery.mailchimp.com/f0f955bc59f272fc94ff6d1a3/images/a522a912-df9c-44cb-9b10-f37617eacd75.jpg" />
    </figure>

    <section>
        <blockquote>
            <p>Validation: “[The] recognition or affirmation that a person and their feelings or opinions are valid or worthwhile.”</p>
            <footer>
                <label for="sn-validation-definition" class="margin-toggle sidenote-number">
                </label>
                <input type="checkbox" id="sn-validation-definition" class="margin-toggle" />
                <span class="sidenote">"Validation, n.3." OED Online. Oxford University Press, January 2018. Web. 27 January 2018
                </span>
            </footer>
        </blockquote>
    </section>
    <section>
        <p>When Arianna (names changed) was in college, she had an idea for a non-profit she wanted to start. She even got a
            fair way through the process with some friends before they all had to go their separate ways and the non-profit
            dissipated into the background.</p>

        <p>Fast forward to a few years later when she was on one of the first dates with her now husband, Cory. Arianna explained
            the idea to Cory as she had many times to many people before. The typical response was, "That's a cool idea."</p>

        <p>This time, however, the response was, "Cool, how would you make that work?" and, "Did you take such-and-such into
            account when planning the model?"</p>

        <p>Arianna's first reaction was to feel attacked and stupid, because she wasn't prepared to answer those solid questions.
            Her second reaction, though, was the first glimpse of falling in love with Cory. He was validating her. And not
            in the buzzword kind of way. </p>

        <p>Validating others and being validated yourself makes a profound difference - especially in a marriage. There are
            many ways to validate your spouse as you Everflect this week, but in this lesson we will focus on three simple
            methods:
            <strong>
                Ask meaningful questions, listen to who they are, and drop the four-letter word</strong>.</p>

    </section>
    <section>
        <h2>Ask Meaningful Questions</h2>

        <p>Meaningful followup questions are one of many ways to communicate to someone that you care and are seeking to understand.
            Love comes about on a deeper level as you get to know someone. It is incredibly difficult to get to know someone
            if you never ask clarifying, validating questions.</p>

        <p>Sometimes we think a simple nod of the head or a verbal thumbs up is validation, but followup questions can add a
            depth of unity to any couple. Arianna realized, through Cory's questions, that he was a man who would go out
            of his way to ensure her dreams
            <em>actually </em>came true. Unlike those who gave her the thumbs up to go it alone, he was taking genuine interest
            and making her life a little more of his own in the process. He took her whimsical idea and put it up against
            questions that would meaningfully validate it and make it possible for future success. Meaningful questions are
            a way to validate who your spouse is by showing them, “I want to get to know you.
            <em>All</em> of you.”</p>

        <p>One note of caution: questions that are meant to interrogate, accuse or disband hopes do not fall under this category.
            Validation can only come in the form of question-asking when it is done from a place of love, sincere interest,
            and integrity. </p>

    </section>
    <section>
        <h2>Listen to Who They Are</h2>

        <p>As defined above, true validation is an affirmation that someone’s feelings or opinions are worthwhile. You cannot
            truly validate someone without seeing them as a person. In other words, we must see them as they actually are
            - a human being with real feelings and real emotions - and we must
            <em>listen</em> instead of only hear them. </p>

        <p>All of those people who told Arianna her non-profit idea was "great" and stopped there may have had good intentions,
            but none of them were validating her or her idea. They were not actually there to listen to her, but to politely
            stand by. Often, as many of us do, they were more than likely resisting Arianna and her non-profit idea in order
            to get on to something they wanted to say about themselves.  </p>
        <blockquote>
            <p>"In the moment we cease resisting others, we're out of the box [and we see people as people]- liberated from
                self-justifying thoughts and feelings."</p>
            <footer>- Leadership and Self Deception
                <label for="sn-resisting-others" class="margin-toggle sidenote-number">
                </label>
                <input type="checkbox" id="sn-resisting-others" class="margin-toggle" />
                <span class="sidenote">
                    <em>Leadership and Self-deception: Getting Out of the Box</em>. San Francisco: Berrett-Koehler, 2000.
                </span>

            </footer>
        </blockquote>

        <p>Validation is liberating.</p>

        <p>One of the reasons Everflect has you write down the things your spouse appreciated about you during the week is to
            help with the validation process. Writing down what your spouse has to say about you is one step toward validating
            their appreciation. Many of us have the tendency to shy away from listening to appreciation, or we get caught
            up too early in what we want to say. By recording what your spouse has to say, you will have a much harder time
            resisting them. This is because you will need to actually
            <em>listen</em> to their appreciation instead of letting it fly past while you are busy prepping the things you would
            like to suggest they do better. </p>

    </section>
    <section>
        <h2>Drop the Four-Letter Word</h2>
        <p>Downplaying someone's ideas, feelings, or actions is also a way in which you are not fully validating them. One simple,
            yet important, skill to gain to help you overcome this is to stop using the word "just." Your spouse is not "just"
            your spouse. They are
            <em>your</em> spouse! When they asked you what you did today, there is no such thing as, "just took the kids to activities
            and went grocery shopping," or, "just went to a bunch of meetings." What meaningful feats those tasks are!</p>

        <blockquote>
            <p>"When referring to an individual&mdash;including yourself&mdash;never use the word 'just'."</p>
            <footer>- Gordon B. Hinckley</footer>
        </blockquote>

        <p>Try to treat this downplaying four-letter word as precisely that - a four-letter word. Remove it from your vocabulary
            - especially when you Everflect. When you are reflecting on your previous week's goals, skip the downplaying
            and take a look at your success. The comment, ”I met my goal of cooking dinner every evening, but they were
            <em>just</em> simple meals" twists the joy of your success into a subconscious failure. So, stop it. You made dinner
            every night!
        </p>

        <p>Especially take note to skip "just" when you are trying to give your spouse a compliment. "I appreciated that you
            brought me flowers this week, even if they were
            <em>just</em> from the grocery store…” suddenly pushed your praise into indirect criticism. Most people only hear
            the "just" part of a sentence anyway. So, drop it. They brought you flowers! Appreciate your spouse more by taking
            the "just" out of the things they do and who they are. </p>

        <p>When you Everflect, it is not "just" a couple's council - it is an "Everflection" - a reflection and inventory of
            your most meaningful relationship in this life. See the beauty in every detail that you discuss. See the success
            (or even the failure) for what it is. It is complex, it is intriguing, and it is great. It matters. It is valid. </p>

    </section>
    <figure class="fullwidth">
        <img src="https://gallery.mailchimp.com/f0f955bc59f272fc94ff6d1a3/images/1aec40fe-fe76-4221-80b2-860f4cbf26ba.jpg" alt="A blank sketchbook with a pencil on a wooden floor"
        />
    </figure>

    <section>
        <h1>Activity</h1>
        <p>This week, go beyond and ask at least one validating question as you write down your spouse's goals and feedback.</p>

        <p>For example, they may say, "I would appreciate it this week if you tried to be home by 6pm, so we can eat dinner
            as a family." </p>

        <p>In response, you could say, "Ok. What I'm hearing is that you feel like me sitting down with the family is important
            at dinner time. I may need to leave earlier in the morning to ensure I can leave work on time to be back, is
            that in line with what you would appreciate from me?”</p>

        <h2>Pro Tip</h2>
        <p>In addition to making sure you don’t use the word “just” to describe yourself or your spouse in your weekly Everflection,
            make a game out of it for the whole week.</p>
        <h3>Rules</h3>
        <ul>
            <li>Don't use the word "just."</li>
            <li>Every time you catch your spouse using the word "just," you get a point (tally it somewhere you both have access).</li>
            <li>Whoever has the most points by your next Everflection has the upper hand when you come to your next difference
                of opinion (such as whose turn it is to do the dishes).</li>
        </ul>
        <hr/>
    </section>
    <section>

        <figure class="fullwidth">
            <img src="https://gallery.mailchimp.com/f0f955bc59f272fc94ff6d1a3/images/99d984f3-9abf-4eb6-9c21-d13ef549d70e.jpg" alt="Piles of closed and open books"
            />
        </figure>
        <h1>Suggested Study</h1>
        <ul>
            <li>
                <em>The Anatomy of Peace</em> &amp;
                <em>Leadership and Self Deception</em> by the Arbinger Institute</li>
        </ul>
    </section>
</article>