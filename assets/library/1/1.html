<article>
    <p>
        Setting goals helps you to have a tangible vision of where you can be. To give you a simple way to do this, Everflect prompts
        you to make new goals every week. Where do you want to be personally, and as a couple, by the time you Everflect
        next week?
        <strong>In this lesson, we will discuss some tips to assist you in writing goals that matter.</strong>
    </p>

    <h1>See Yourself</h1>
    <p class="caption">Set Meaningful Goals</p>
    <figure class="fullwidth">
        <img src="https://gallery.mailchimp.com/f0f955bc59f272fc94ff6d1a3/images/71e44e5f-9170-44b5-8926-bfc42ba6c489.jpeg" alt="A couple relaxes side by side from the back of a car overlooking wide open fields."
        />
    </figure>
    <hr/>
    <section>
        <p>
            S.M.A.R.T. (Specific, Measurable, Attainable, Relevant and Time-Based) is a common acronym used to describe constraints that
            can influence the effectiveness of a written goal. “I will not get on Facebook after 8pm each night," is an example
            of a goal with a "S.M.A.R.T." foundation. It is
            <strong>specific</strong> by referring to a particular social media (Facebook) instead of making a blanket statement.
            It is
            <strong>measurable</strong>, because success is defined as a solid "Yes, I got on Facebook" or "No, I did not get on
            Facebook." It is
            <strong>attainable</strong> by being in your control.  It is
            <strong>relevant</strong>, because social media can have effects that influence your relationship. It is
            <strong>time-based</strong> by using a specific time (8pm) and day (each night).  All of this is a great start. If your
            goals are S.M.A.R.T. you’re on a pretty good foundation. The problem is, no matter how motivated you are at keeping
            a healthy goal like this, your mind may not
            <em>think</em> you can. There are, however, a couple of changes that can be made that will have an even greater influence
            on your success: stay positive and stay current.</p>
    </section>

    <section>
        <h2>1. Stay Positive</h2>
        <p>Don't think about a purple elephant. Oh wait, you just did.</p>
        <p>
            Similar to this psychological phenomenon, even simple words can spark reactions in your brain. A negative word in a sentence,
            for example, can trigger fear or disbelief. In order to make a meaningful goal, your mind needs to be able to
            envision your success without it being connected to fearful hesitation. The simple “not” in the example goal and the
            "don't" in the sentence above are negative words. This is what your mind focuses on and reacts to first. It may
            be triggering negative emotions such as fear, hopelessness and sorrow.</p>
        <p>
            A positive change to the example goal such as, “I will only get on Facebook between noon and 8pm each day” can turn those
            negative triggers into positive motivators. Now, your mind can focus on what you
            <em>can </em>do, instead of what you might
            <em>not </em>do. This goal is now an intentional goal - rather than being a written attempt to avoid a negative habit,
            this goal now gives you a clear vision of what you are capable of and what perimeters you need in order to act.
            By making this simple change, you replace fear and uncertainty with hope and faith.
        </p>
    </section>

    <section>
        <h2>2. Stay Current</h2>

        <blockquote>
            <p>"[Keeping the end in mind] is based on imagination--the ability to envision in your mind what you cannot at present
                see with your eyes."</p>
            <footer>- Stephen R Covey
                <label for="sn-stay-current" class="margin-toggle sidenote-number">
                </label>
                <input type="checkbox" id="sn-stay-current" class="margin-toggle" />
                <span class="sidenote">Covey, Stephen R. The 7 Habits of Highly Effective People: Restoring the Character Ethic</em>. [Rev. ed.].
                    New York: Free Press, 2004.</span>
            </footer>
        </blockquote>

        <p>In order for your subconscious to believe you can achieve your goals, it helps to be able to see what it would be
            like to succeed
            <strong>now.</strong> Your brain cares about the present tense. The word “will” in the above goal suggests to you that
            success is forever in the future. Instead, you could say “I only get on Facebook between noon and 8pm each day.”
            The goal is now a part of you instead of a part of your future. You are more likely to succeed with your goals
            at a quicker rate now, because you can clearly "keep the end in mind" as if you are already there.</p>

        <p>
            As you set your goals while you Everflect this week, see yourself achieving them and start now to be the person you see. Taking
            the time to see yourself as the couple you want to become gives you a stepping stone toward making that beautiful
            vision a reality.
        </p>

        <figure class="fullwidth">
            <img src="https://gallery.mailchimp.com/f0f955bc59f272fc94ff6d1a3/images/1aec40fe-fe76-4221-80b2-860f4cbf26ba.jpg" alt="A blank sketchbook with a pencil on a wooden floor"
            />
        </figure>
    </section>
    <section>
        <h1>Activity</h1>

        <p>Before you Everflect this week, create a couple's mission statement describing who you want to be as a couple. You
            can also write one for yourself, but make sure to share it with your spouse afterwards. </p>

        <p>How to:</p>
        <p>Write at least a 5 sentence description of who you would like to be as a couple.  </p>
        <p>Remember these two rules: </p>

        <ol>
            <li>Only use
                <strong>positive</strong> wording</li>
            <li>Keep it in the
                <strong>present tense</strong>
            </li>
        </ol>

        <h3>Example:</h3>
        <blockquote>
            <p>We prioritize our marriage. We Everflect every Sunday at 7pm to take inventory of our relationship and support
                each other in our goals. We go on a planned date night every Friday to reconnect and enjoy life together.
                We plan and review a budget each month to stay on the same page with our finances. We value meaningful communication
                and set aside the last hour before bed each night to engage in uplifting conversations. We are deeply in
                love, because we chose every day to be so. Our marriage matters.</p>
        </blockquote>

        <h2>Pro tip:</h2>
        <p>Type your couple’s mission statement up, frame it and put in in your room. Read it often. Review and revise it together
            once a year on a couple’s retreat.</p>
        <p>If you would like to share your couple's mission statement with us, please feel free to e-mail us at
            <a href="mailto:hello@everflect.com">hello@everflect.com</a> or tag us in a social media post.
        </p>

        <figure class="fullwidth">
            <img src="https://gallery.mailchimp.com/f0f955bc59f272fc94ff6d1a3/images/99d984f3-9abf-4eb6-9c21-d13ef549d70e.jpg" alt="Piles of closed and open books"
            />
        </figure>
    </section>
    <section>
        <h1>Suggested Study</h1>
        <ul>
            <li>
                <em>7 Habits of Highly Effective Marriage</em> by Dr. Stephen R. Covey</li>
        </ul>

        <figure class="fullwidth">
            <img src="https://gallery.mailchimp.com/f0f955bc59f272fc94ff6d1a3/images/eeee3931-932c-4c6d-9380-1f0b35fd1b8e.jpg" alt="A road through a red rock wilderness"
            />
        </figure>
    </section>
</article>