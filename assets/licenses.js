module.exports = `
<h1>
    <a id="Everflect_Licenses_0"></a>Everflect Licenses</h1>
<p>Everflect for Couples by Coriaria, LLC (like most software written today) benefits greatly from open source contributions
    made by organizations and individuals from around the world. Coriaria, LLC also makes contributions to open source initiatives
    where possible. The following is the collection of licenses for open source software used in Everflect Couples.
</p>
<p>
    <a href="https://github.com/apollographql/apollo-client">apollo-client</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2015 - 2016 Meteor Development Group, Inc.</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/apollographql/apollo-link">apollo-link</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2016 - 2017 Meteor Development Group, Inc.</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/apollostack/graphql-tag">graphql-tag</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2015 - 2016 Meteor Development Group, Inc.</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/apollostack/react-apollo">react-apollo</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2015 Ben Newman</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/archriss/react-native-render-html">react-native-render-html</a>
</p>
<p>Copyright © 2016, Maxime Bertonnier
    <br> All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without
    <br> modification, are permitted provided that the following conditions are met:</p>
<ul>
    <li>
        <p>Redistributions of source code must retain the above copyright notice, this
            <br> list of conditions and the following disclaimer.</p>
    </li>
    <li>
        <p>Redistributions in binary form must reproduce the above copyright notice,
            <br> this list of conditions and the following disclaimer in the documentation
            <br> and/or other materials provided with the distribution.</p>
    </li>
</ul>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
    <br> AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    <br> IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    <br> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    <br> FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    <br> DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    <br> SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    <br> CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    <br> OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    <br> OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
<p>
    <a href="https://github.com/archriss/react-native-snap-carousel">react-native-snap-carousel</a>
</p>
<p>BSD 3-Clause License</p>
<p>Copyright © 2017, Archriss
    <br> All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without
    <br> modification, are permitted provided that the following conditions are met:</p>
<ul>
    <li>
        <p>Redistributions of source code must retain the above copyright notice, this
            <br> list of conditions and the following disclaimer.</p>
    </li>
    <li>
        <p>Redistributions in binary form must reproduce the above copyright notice,
            <br> this list of conditions and the following disclaimer in the documentation
            <br> and/or other materials provided with the distribution.</p>
    </li>
    <li>
        <p>Neither the name of the copyright holder nor the names of its
            <br> contributors may be used to endorse or promote products derived from
            <br> this software without specific prior written permission.</p>
    </li>
</ul>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
    <br> AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    <br> IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    <br> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    <br> FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    <br> DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    <br> SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    <br> CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    <br> OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    <br> OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
<p>
    <a href="https://github.com/eslint/eslint">eslint</a>
</p>
<p>Copyright JS Foundation and other contributors,
    <a href="https://js.foundation">https://js.foundation</a>
</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in
    <br> all copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    <br> THE SOFTWARE.</p>
<p>
    <a href="https://github.com/expo/eslint-config-expo">eslint-config-expo</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2016 Expo</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/expo/expo-sdk">expo</a>
</p>
<p>BSD License</p>
<p>For Exponent software</p>
<p>Copyright © 2015-present, 650 Industries, Inc. All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without modification,
    <br> are permitted provided that the following conditions are met:</p>
<ul>
    <li>
        <p>Redistributions of source code must retain the above copyright notice, this
            <br> list of conditions and the following disclaimer.</p>
    </li>
    <li>
        <p>Redistributions in binary form must reproduce the above copyright notice,
            <br> this list of conditions and the following disclaimer in the documentation
            <br> and/or other materials provided with the distribution.</p>
    </li>
    <li>
        <p>Neither the names 650 Industries, Exponent, nor the names of its contributors
            <br> may be used to endorse or promote products derived from this software without
            <br> specific prior written permission.</p>
    </li>
</ul>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND
    <br> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    <br> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    <br> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
    <br> ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    <br> (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    <br> LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    <br> ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    <br> (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    <br> SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
<p>
    <a href="https://github.com/facebook/react-native">react-native</a>
</p>
<p>BSD License</p>
<p>For React Native software</p>
<p>Copyright © 2015-present, Facebook, Inc. All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without modification,
    <br> are permitted provided that the following conditions are met:</p>
<ul>
    <li>
        <p>Redistributions of source code must retain the above copyright notice, this
            <br> list of conditions and the following disclaimer.</p>
    </li>
    <li>
        <p>Redistributions in binary form must reproduce the above copyright notice,
            <br> this list of conditions and the following disclaimer in the documentation
            <br> and/or other materials provided with the distribution.</p>
    </li>
    <li>
        <p>Neither the name Facebook nor the names of its contributors may be used to
            <br> endorse or promote products derived from this software without specific
            <br> prior written permission.</p>
    </li>
</ul>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND
    <br> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    <br> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    <br> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
    <br> ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    <br> (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    <br> LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    <br> ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    <br> (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    <br> SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
<p>
    <a href="https://github.com/facebook/react">react</a>
</p>
<p>MIT License</p>
<p>Copyright © 2013-present, Facebook, Inc.</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/gcanti/tcomb-form-native">tcomb-form-native</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2015 Giulio Canti</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/graphql/graphql-js">graphql</a>
</p>
<p>MIT License</p>
<p>Copyright © 2015-present, Facebook, Inc.</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/hectahertz/react-native-typography">react-native-typography</a>
</p>
<p>MIT License</p>
<p>Copyright © 2017 Hector Garcia</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/kunalgolani/eslint-config">eslint-config-react-native</a>
</p>
<p>Copyright 2018</p>
<p>Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted,
    provided that the above copyright notice and this permission notice appear in all copies.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS” AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT,
    OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
    OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
    THIS SOFTWARE.</p>
<p>
    <a href="https://github.com/marionebl/commitlint">@commitlint/cli</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2016 Mario Nebl</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/moment/moment">moment</a>
</p>
<p>Copyright © JS Foundation and other contributors</p>
<p>Permission is hereby granted, free of charge, to any person
    <br> obtaining a copy of this software and associated documentation
    <br> files (the “Software”), to deal in the Software without
    <br> restriction, including without limitation the rights to use,
    <br> copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the
    <br> Software is furnished to do so, subject to the following
    <br> conditions:
</p>
<p>The above copyright notice and this permission notice shall be
    <br> included in all copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
    <br> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    <br> OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    <br> NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    <br> HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    <br> WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    <br> FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    <br> OTHER DEALINGS IN THE SOFTWARE.</p>
<p>
    <a href="https://github.com/prettier/eslint-config-prettier">eslint-config-prettier</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2017 Simon Lydell</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in
    <br> all copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    <br> THE SOFTWARE.</p>
<p>
    <a href="https://github.com/prettier/eslint-plugin-prettier">eslint-plugin-prettier</a>
</p>
<h1>
    <a id="The_MIT_License_MIT_487"></a>The MIT License (MIT)</h1>
<p>Copyright © 2017 Andres Suarez and Teddy Katz</p>
<p>Permission is hereby granted, free of charge, to any person
    <br> obtaining a copy of this software and associated documentation
    <br> files (the “Software”), to deal in the Software without
    <br> restriction, including without limitation the rights to use,
    <br> copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the
    <br> Software is furnished to do so, subject to the following
    <br> conditions:
</p>
<p>The above copyright notice and this permission notice shall be
    <br> included in all copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
    <br> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    <br> OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    <br> NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    <br> HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    <br> WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    <br> FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    <br> OTHER DEALINGS IN THE SOFTWARE.</p>
<p>
    <a href="https://github.com/prettier/prettier">prettier</a>
</p>
<p>Copyright 2017-2018 James Long</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
    files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
    whom the Software is furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
    Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
    OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
    OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
</p>
<p>
    <a href="https://github.com/Purii/react-native-tableview-simple">react-native-tableview-simple</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2015-present Patrick Puritscher</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/react-community/create-react-native-app">react-native-scripts</a>
</p>
<p>Copyright © 2017, Create React Native App Contributors
    <br> All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without
    <br> modification, are permitted provided that the following conditions are met:</p>
<ul>
    <li>
        <p>Redistributions of source code must retain the above copyright notice, this
            <br> list of conditions and the following disclaimer.</p>
    </li>
    <li>
        <p>Redistributions in binary form must reproduce the above copyright notice,
            <br> this list of conditions and the following disclaimer in the documentation
            <br> and/or other materials provided with the distribution.</p>
    </li>
    <li>
        <p>Neither the name of the copyright holder nor the names of its
            <br> contributors may be used to endorse or promote products derived from
            <br> this software without specific prior written permission.</p>
    </li>
</ul>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
    <br> AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    <br> IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    <br> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    <br> FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    <br> DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    <br> SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    <br> CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    <br> OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    <br> OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
<p>
    <a href="https://github.com/react-community/react-navigation">react-navigation</a>
</p>
<p>BSD License</p>
<p>For React Navigation software</p>
<p>Copyright © 2016-present, React Navigation Contributors. All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without modification,
    <br> are permitted provided that the following conditions are met:</p>
<ul>
    <li>
        <p>Redistributions of source code must retain the above copyright notice, this
            <br> list of conditions and the following disclaimer.</p>
    </li>
    <li>
        <p>Redistributions in binary form must reproduce the above copyright notice,
            <br> this list of conditions and the following disclaimer in the documentation
            <br> and/or other materials provided with the distribution.</p>
    </li>
</ul>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND
    <br> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    <br> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    <br> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
    <br> ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    <br> (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    <br> LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    <br> ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    <br> (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    <br> SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
<p>
    <a href="https://github.com/reactjs/prop-types">prop-types</a>
</p>
<p>MIT License</p>
<p>Copyright © 2013-present, Facebook, Inc.</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/reactjs/react-redux">react-redux</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2015-present Dan Abramov</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/reactjs/redux">redux</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2015-present Dan Abramov</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/typicode/husky">husky</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2014</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/xgfe/react-native-datepicker">react-native-datepicker</a>
</p>
<p>The MIT License (MIT)</p>
<p>Copyright © 2016 鲜果FE</p>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy
    <br> of this software and associated documentation files (the “Software”), to deal
    <br> in the Software without restriction, including without limitation the rights
    <br> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    <br> copies of the Software, and to permit persons to whom the Software is
    <br> furnished to do so, subject to the following conditions:</p>
<p>The above copyright notice and this permission notice shall be included in all
    <br> copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    <br> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    <br> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    <br> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    <br> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    <br> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    <br> SOFTWARE.
</p>
<p>
    <a href="https://github.com/expo/sentry-expo">sentry-expo</a>
</p>
<p>BSD License</p>
<p>For Exponent software</p>
<p>Copyright © 2015-present, 650 Industries, Inc. All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without modification,
    <br> are permitted provided that the following conditions are met:</p>
<ul>
    <li>
        <p>Redistributions of source code must retain the above copyright notice, this
            <br> list of conditions and the following disclaimer.</p>
    </li>
    <li>
        <p>Redistributions in binary form must reproduce the above copyright notice,
            <br> this list of conditions and the following disclaimer in the documentation
            <br> and/or other materials provided with the distribution.</p>
    </li>
    <li>
        <p>Neither the names 650 Industries, Exponent, nor the names of its contributors
            <br> may be used to endorse or promote products derived from this software without
            <br> specific prior written permission.</p>
    </li>
</ul>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND
    <br> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    <br> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    <br> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
    <br> ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    <br> (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    <br> LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    <br> ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    <br> (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    <br> SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
<p>
    <a href="https://github.com/brentvatne/eslint-config-react-native-prettier">eslint-config-react-native-prettier</a>
</p>
<p>BSD License</p>
<p>For Exponent software</p>
<p>Copyright © 2015-present, 650 Industries, Inc. All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without modification,
    <br> are permitted provided that the following conditions are met:</p>
<ul>
    <li>
        <p>Redistributions of source code must retain the above copyright notice, this
            <br> list of conditions and the following disclaimer.</p>
    </li>
    <li>
        <p>Redistributions in binary form must reproduce the above copyright notice,
            <br> this list of conditions and the following disclaimer in the documentation
            <br> and/or other materials provided with the distribution.</p>
    </li>
    <li>
        <p>Neither the names 650 Industries, Exponent, nor the names of its contributors
            <br> may be used to endorse or promote products derived from this software without
            <br> specific prior written permission.</p>
    </li>
</ul>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND
    <br> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    <br> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    <br> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
    <br> ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    <br> (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    <br> LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    <br> ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    <br> (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    <br> SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
<p>
    <a href="https://github.com/expo/jest-expo">jest-expo</a>
</p>
<p>BSD License</p>
<p>For Exponent software</p>
<p>Copyright © 2015-present, 650 Industries, Inc. All rights reserved.</p>
<p>Redistribution and use in source and binary forms, with or without modification,
    <br> are permitted provided that the following conditions are met:</p>
<ul>
    <li>
        <p>Redistributions of source code must retain the above copyright notice, this
            <br> list of conditions and the following disclaimer.</p>
    </li>
    <li>
        <p>Redistributions in binary form must reproduce the above copyright notice,
            <br> this list of conditions and the following disclaimer in the documentation
            <br> and/or other materials provided with the distribution.</p>
    </li>
    <li>
        <p>Neither the names 650 Industries, Exponent, nor the names of its contributors
            <br> may be used to endorse or promote products derived from this software without
            <br> specific prior written permission.</p>
    </li>
</ul>
<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND
    <br> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    <br> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    <br> DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
    <br> ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    <br> (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    <br> LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    <br> ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    <br> (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    <br> SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
`;
