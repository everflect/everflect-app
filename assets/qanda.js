module.exports = `
<h1>Commonly asked questions</h1>
<h2>What is a “Couple’s Council”?</h2>
<p>Couple’s councils can come in many forms. In short, a couple’s council is a consistent opportunity to sit down as a couple
    (married or dating) and reflect on your relationship. Everflect’s version of a couple’s council is based off of the LDS
    missionary “Companionship Inventory” which goes as follows:</p>

<blockquote>“At the end of your weekly planning session, share with your companion appropriate goals, and ask for his or her help to
    accomplish them. Discuss the strength of your relationship with your companion. Discuss any challenges that may be keeping
    your companionship from working in unity or from being obedient. Resolve conflicts. Share with your companion what you
    think his or her strengths are. Ask for suggestions on how you can improve. If needed, set goals that will improve your
    relationship. Conclude with prayer”<footer>Preach My Gospel: A Guide to Missionary Service, (2004), 137–54</footer></blockquote>
    
<p>Everflect adapts this version to be specifically applicable to couples and is implemented through a method that draws from
    academic studies on constructive communication.</p>


<h2>Isn’t it counter-productive to use technology to try to improve communication?</h2>
<p>This is an important question. Everflect is written in such a way that you have to verbally communicate with your spouse
    as well as actively listen. Everflect also gives you a tool to keep track of goals and remind you to follow up with your
    spouse. If you and your spouse get into the habit of “Everflecting” and feel that you could continue to do so without
    the help of the app, we encourage you to do so.</p>


<h2>Is Everflect a replacement for professional marriage counseling?</h2>
<p> No. Everflect is a tool for couples to use and can go hand-in-hand with professional counseling, but is not intended to replace
    it. We also work to provide well-researched advice within our 10-week course, but we encourage couples to seek out professional
    help if needed. (If you are a professional and would like to get in touch with us, please fill out our contact form)
</p>


<h2>Is Everflect free?</h2>
<p>Yes! The initial app will be free. The 10-week course as well as future upgrades will be optional purchases.</p>


<h2>Who is the intended audience for Everflect?</h2>
<p>Everflect has been written with a “traditional” married couple in mind. The 10-week course talks about “spouses” and “marriage”,
    but the information is also applicable to couples in various situations. Those who are
    <em>most likely</em> to benefit from Everflect are heterosexual couples who have a “good” relationship now, but are seeking
    ways to improve their communication and strengthen their marriage. However, we do not discriminate against any couple
    who would like to use Everflect to improve their relationship. Anyone is welcome to use our apps &amp; products.</p>


<h2>How will Everflect help my relationship?</h2>
<p> Communication is critical in healthy relationships. Everflect aims to provide you with a tool that will assist you in having
    a meaningful discussion with your spouse every week and stay accountable to each other. Our optional course work also
    provides you with bite-sized portions of well-researched advice and gives suggested relationship building activities
    to try with your spouse. Everflect requires willingness from both spouses to be effective, and the success of Everflect
    depends on your personal application of the app and course-work.</p>


<h2>Is this religious?</h2>
<p>Not specifically. Everflect was inspired by the experience of
    <a href="https://www.mormon.org/missionaries?">LDS religious missionaries</a>, but the app is intended for use by couples with diverse backgrounds. Resources for the
    course work are pulled from religious leaders (of various faiths) as well as academic studies.</p>


<h2>What will be included in the 10-week course?</h2>
<p>The 10-week course is a collection of relationship advice that is specifically applicable to doing a couple’s council like
    Everflect, but it is easily applied to other everyday life scenarios. Examples of topics that will be covered in the
    lessons are vulnerability, communication, balance, love languages, etc. The lessons also include relationship building
    activities as well as suggested further reading.</p>


<h2>Why does it take so long to hear back or get support from Everflect™?</h2>
<p>Here at Everflect we believe that family is first. Because of that, we have very strict working “office hours”. We work hard
    during those hours to make sure we are able to assist you in any way possible, but some replies may be delayed for family
    matters. Thank you for your understanding, support and patience!
</p>

<hr/>
<p>Have additional questions? Feel free to send them to us at <a href="mailto:hello@everflect.com">hello@everflect.com</a> or reaching out on one of our social media
    pages.</p>

    <p>Made with ❤️ by Coriaria, LLC</p>

`;
