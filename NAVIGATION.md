# The base of the app presents the primary controller for the app, including modals
/root

# Onboarding is a modal stackrouter with multiple pages
/root/onboarding/index
/root/onboarding/login
/root/onboarding/login/forgotPassword
/root/onboarding/register/account
/root/onboarding/register/subscription

# App Tips is a modal stackrouter with a carousel
# The carousel is populated with individual app tips
/root/appTips

# Inventory steps themselves
/root/inventory/:inventory_id/:step

# Root Tab navigator for main app interaction
/root/home

# Stack Navigator showing list of inventories,
#   current spouse status, if there is an active
#   inventory, etc. Active inventories triggered
#   up to the /root controller
/root/home/inventories
/root/home/inventories/:inventory_id

# Stack navigator with miscellaneous settings
/root/home/settings

# Stack navigator with content, including purchasing
/root/home/library
/root/home/library/:course
/root/home/library/:course/:section

# Stack navigator for managing in-app purchases/billing
/root/subscriptions