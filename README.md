# Everflect App

## iOS error: "React/RCTDefines.h" file not found

https://github.com/dooboolab/react-native-iap/issues/6

Update Header Search Paths for sub-project in question to:
`$(SRCROOT)/../../../ios/Pods/Headers/Public`
